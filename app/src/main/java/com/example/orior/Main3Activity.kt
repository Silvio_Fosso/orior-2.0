package com.example.orior

import android.content.Intent
import android.os.Bundle
import android.widget.LinearLayout
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.orior.Adapter.RvAdapter2
import com.example.orior.DataCard.datiCard2
import com.example.orior.okHttp.okHttp1


class Main3Activity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main2)

        supportActionBar?.setTitle("Regioni")

       // supportActionBar?.setDisplayHomeAsUpEnabled(true)
        val recyclerView = findViewById<RecyclerView>(R.id.Recyclevie)
        recyclerView.layoutManager = LinearLayoutManager(this, LinearLayout.VERTICAL, false)
        val dataList = ArrayList<datiCard2>()
        okHttp1.shared.getDatiRegionaliLatest { s->
            s.forEach {
                dataList.add(
                    datiCard2(
                        it.denominazione_regione,
                        it.totale_casi.toString(),
                        it.deceduti.toString(),
                        it.dimessi_guariti.toString()
                    )
                )
            }
            runOnUiThread {
                val rvAdapter = RvAdapter2(dataList)

                recyclerView.adapter = rvAdapter
                rvAdapter.onItemClick = { pos, view ->
                    val b = Bundle()
                    val intent = Intent(this.applicationContext, Main4Activity::class.java)

                    b.putString("regione",dataList.get(pos).regione) //Your id
                    intent.putExtras(b) //Put your id to your next Intent
                    startActivity(intent)
                }


            }
        }


    }
}
