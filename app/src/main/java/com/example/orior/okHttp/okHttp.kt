package com.example.orior.okHttp

import com.example.orior.DatiJson.DatiNazionali
import com.example.orior.DatiJson.DatiProvincia
import com.example.orior.DatiJson.DatiRegionali
import com.example.orior.DatiJson.News
import com.example.orior.ui.Contatti.DatiTuttiNumeri
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import okhttp3.*
import java.io.IOException
import java.lang.reflect.Type




class okHttp1 {
    val okHttpClient = OkHttpClient()
    fun getDatiNazionali(callback: (Collection<DatiNazionali>) -> Unit){

        val request = Request.Builder()
            .url("https://raw.githubusercontent.com/pcm-dpc/COVID-19/master/dati-json/dpc-covid19-ita-andamento-nazionale.json")
            .build()
        okHttpClient.newCall(request).enqueue(object : Callback {
            override fun onFailure(call: Call, e: IOException) {

            }

            override fun onResponse(call: Call, response: Response) {
                // Handle this
                val collectionType: Type? = object :
                    TypeToken<Collection<DatiNazionali?>?>() {}.type
                val enums: Collection<DatiNazionali> =
                    Gson().fromJson(response.body?.string(), collectionType)
                response.body?.close()
                callback(enums)

            }
        })
    }



    fun getDatiRegionaliLatest(callback: (Collection<DatiRegionali>) -> Unit){

        val request = Request.Builder()
            .url("https://raw.githubusercontent.com/pcm-dpc/COVID-19/master/dati-json/dpc-covid19-ita-regioni-latest.json")
            .build()
        okHttpClient.newCall(request).enqueue(object : Callback {
            override fun onFailure(call: Call, e: IOException) {

            }

            override fun onResponse(call: Call, response: Response) {
                // Handle this
                val collectionType: Type? = object :
                    TypeToken<Collection<DatiRegionali?>?>() {}.type
                val enums: Collection<DatiRegionali> =
                    Gson().fromJson(response.body?.string(), collectionType)
                response.body?.close()
                callback(enums)

            }
        })
    }

    fun getDatiEuropeiConfermati(callback: (String?) -> Unit){

        val request = Request.Builder()
            .url("https://raw.githubusercontent.com/CSSEGISandData/COVID-19/master/csse_covid_19_data/csse_covid_19_time_series/time_series_covid19_confirmed_global.csv")
            .build()
        okHttpClient.newCall(request).enqueue(object : Callback {
            override fun onFailure(call: Call, e: IOException) {

            }

            override fun onResponse(call: Call, response: Response) {
                // Handle this

                    callback(response.body?.string())

            }
        })
    }
    fun getDatiEuropeiGuariti(callback: (String?) -> Unit){

        val request = Request.Builder()
            .url("https://raw.githubusercontent.com/CSSEGISandData/COVID-19/master/csse_covid_19_data/csse_covid_19_time_series/time_series_covid19_recovered_global.csv")
            .build()
        okHttpClient.newCall(request).enqueue(object : Callback {
            override fun onFailure(call: Call, e: IOException) {

            }

            override fun onResponse(call: Call, response: Response) {
                // Handle this

                callback(response.body?.string())

            }
        })
    }
    fun getDatiEuropeiDeceduti(callback: (String?) -> Unit){

        val request = Request.Builder()
            .url("https://raw.githubusercontent.com/CSSEGISandData/COVID-19/master/csse_covid_19_data/csse_covid_19_time_series/time_series_covid19_deaths_global.csv")
            .build()
        okHttpClient.newCall(request).enqueue(object : Callback {
            override fun onFailure(call: Call, e: IOException) {

            }

            override fun onResponse(call: Call, response: Response) {
                // Handle this

                callback(response.body?.string())

            }
        })
    }


    fun getNews(ln : String,callback: (Collection<News>) -> Unit){


      //  The current accepted answer is out of date. Now if you want to create a post request and add parameters to it you should user MultipartBody.Builder as Mime Craft now is deprecated.

        var requestBody = MultipartBody.Builder()
            .setType(MultipartBody.FORM)
            .addFormDataPart("ln", ln)
            .build();

        val request = Request.Builder()
            .url("https://www.orior-manager.cloud/News.php")
            .build()

        okHttpClient.newCall(request).enqueue(object : Callback {
            override fun onFailure(call: Call, e: IOException) {

            }

            override fun onResponse(call: Call, response: Response) {
                // Handle this
                val collectionType: Type? = object :
                    TypeToken<Collection<News?>?>() {}.type
                val enums: Collection<News> =
                    Gson().fromJson(response.body?.string(), collectionType)
                response.body?.close()
                callback(enums)

            }
        })
    }

    fun getDatiRegionali(callback: (Collection<DatiRegionali>) -> Unit){

        val request = Request.Builder()
            .url("https://raw.githubusercontent.com/pcm-dpc/COVID-19/master/dati-json/dpc-covid19-ita-regioni.json")
            .build()
        okHttpClient.newCall(request).enqueue(object : Callback {
            override fun onFailure(call: Call, e: IOException) {

            }

            override fun onResponse(call: Call, response: Response) {
                // Handle this
                val collectionType: Type? = object :
                    TypeToken<Collection<DatiRegionali?>?>() {}.type
                val enums: Collection<DatiRegionali> =
                    Gson().fromJson(response.body?.string(), collectionType)
                response.body?.close()
                callback(enums)

            }
        })
    }




    fun getDatiProvincia(callback: (Collection<DatiProvincia>) -> Unit){

        val request = Request.Builder()
            .url("https://raw.githubusercontent.com/pcm-dpc/COVID-19/master/dati-json/dpc-covid19-ita-province.json")
            .build()
        okHttpClient.newCall(request).enqueue(object : Callback {
            override fun onFailure(call: Call, e: IOException) {

            }

            override fun onResponse(call: Call, response: Response) {
                // Handle this
                val collectionType: Type? = object :
                    TypeToken<Collection<DatiProvincia?>?>() {}.type
                val enums: Collection<DatiProvincia> =
                    Gson().fromJson(response.body?.string(), collectionType)
                response.body?.close()
                callback(enums)

            }
        })
    }

    fun getDatiProvinciaLatest(callback: (Collection<DatiProvincia>) -> Unit){

        val request = Request.Builder()
            .url("https://raw.githubusercontent.com/pcm-dpc/COVID-19/master/dati-json/dpc-covid19-ita-province-latest.json")
            .build()
        okHttpClient.newCall(request).enqueue(object : Callback {
            override fun onFailure(call: Call, e: IOException) {

            }

            override fun onResponse(call: Call, response: Response) {
                // Handle this
                val collectionType: Type? = object :
                    TypeToken<Collection<DatiProvincia?>?>() {}.type
                val enums: Collection<DatiProvincia> =
                    Gson().fromJson(response.body?.string(), collectionType)

                callback(enums)

            }
        })
    }
    fun getDatiNumeriRegionali(callback: (DatiTuttiNumeri) -> Unit){

        val request = Request.Builder()
            .url("https://raw.githubusercontent.com/andryg98/Covid-19/master/covid_numbers.json")
            .build()
        okHttpClient.newCall(request).enqueue(object : Callback {
            override fun onFailure(call: Call, e: IOException) {

            }

            override fun onResponse(call: Call, response: Response) {


                val numeri = Gson().fromJson(response.body?.string(),
                    DatiTuttiNumeri::class.java)

                callback(numeri)

            }
        })
    }




    companion object {
        val shared = okHttp1()
    }


}