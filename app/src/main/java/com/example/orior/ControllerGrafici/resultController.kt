package com.example.orior.ControllerGrafici

import CustomMarker
import android.content.Context
import android.graphics.Color
import android.os.Build
import android.view.View
import android.widget.Button
import androidx.annotation.RequiresApi
import androidx.core.view.size
import com.example.orior.DatiJson.Csv.DatiArray
import com.example.orior.DatiJson.DatiNazionali
import com.example.orior.DatiJson.DatiProvincia
import com.example.orior.DatiJson.DatiRegionali
import com.example.orior.R
import com.github.mikephil.charting.charts.BarChart
import com.github.mikephil.charting.charts.LineChart
import com.github.mikephil.charting.charts.PieChart
import com.github.mikephil.charting.components.Legend
import com.github.mikephil.charting.components.XAxis
import com.github.mikephil.charting.components.YAxis
import com.github.mikephil.charting.data.*
import com.github.mikephil.charting.formatter.ValueFormatter
import com.github.mikephil.charting.highlight.Highlight
import com.github.mikephil.charting.listener.OnChartValueSelectedListener
import com.github.mikephil.charting.renderer.XAxisRenderer
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.TimeUnit
import kotlin.collections.ArrayList
import kotlin.concurrent.schedule
import kotlin.math.abs


class chartController : OnChartValueSelectedListener {
    lateinit var grafico : LineChart
    lateinit var grafico1 : PieChart

      lateinit var timer : TimerTask
    lateinit var timer1 : TimerTask
    constructor(naz : Collection<DatiNazionali>?, graph : LineChart){
        getLineeChart(naz, graph)
        timer = Timer().schedule(0){}
    }

    constructor(naz : Collection<DatiRegionali>?, graph : PieChart ){
        graficorotodo(graph,naz)
        timer1 = Timer().schedule(0){}
        this.grafico1 = graph
    }

    constructor(reg : Collection<DatiRegionali>?, grafico : LineChart, nomeRegione : String,contesto : Context){
        getLineeChartRegione(reg,grafico,nomeRegione,contesto)
        timer = Timer().schedule(0){}
        this.grafico = grafico
    }
    constructor(reg : DatiArray, grafico : LineChart, nomestato : String,contesto : Context){
        getLineeChartEuropaStato(reg,grafico,nomestato,contesto)
        timer = Timer().schedule(0){}
        this.grafico = grafico
    }
    constructor(prov : Collection<DatiProvincia>?, grafico : PieChart, nomeRegione : String){
        graficoRotondoRegione(grafico,prov,nomeRegione)
        timer1 = Timer().schedule(0){}
        this.grafico1 = grafico

    }
    constructor(naz : Collection<DatiNazionali>?, graph: BarChart, scelta : Int,btn1:Button,btn2:Button,btn3:Button,context : Context){
        getBarChart(naz,graph,scelta,btn1,btn2,btn3,context)
    }
    constructor(reg : Collection<DatiRegionali>?, graph : BarChart, nomeRegione : String, scelta : Int,contesto : Context,btn1:Button,btn2:Button,btn3:Button){
    getBarChartRegione(reg,graph,scelta,nomeRegione,contesto,btn1,btn2,btn3)
    }

    constructor(dat : DatiArray,graph : LineChart,context : Context){

        getLineeChartEuropeo(dat, graph,context)
        timer = Timer().schedule(0){}
        this.grafico = grafico

    }
    constructor(dat : DatiArray,graph : PieChart,context : Context){
        graficorotodoEuropeo(graph,dat,context)
        timer1 = Timer().schedule(0){}
        this.grafico1 = graph

    }
    constructor(dat : DatiArray,graph : BarChart,scelta : Int,btn1:Button,btn2:Button,btn3:Button,context : Context){
        getBarChartEuropeo(dat,graph,scelta,btn1,btn2,btn3,context)

    }
    constructor(dat : DatiArray,graph : BarChart,scelta : Int,btn1:Button,btn2:Button,btn3:Button,context : Context,stato : String){
        getBarChartEuropeoStato(dat,graph,scelta,btn1,btn2,btn3,context,stato)

    }
    constructor(dat : DatiArray,graph : PieChart,context : Context,stato : String){
    graficorotodoEuropeoStato(graph,dat,context,stato)
        timer1 = Timer().schedule(0){}
        this.grafico1 = graph
    }


    fun getBarChart(nazione : Collection<DatiNazionali>?, graph: BarChart, scelta: Int,btn1:Button,btn2:Button,btn3:Button,contesto : Context){
        graph.legend.form = Legend.LegendForm.EMPTY
        graph.axisLeft.isEnabled = false
        graph.xAxis.position =XAxis.XAxisPosition.BOTTOM
        graph.legend.horizontalAlignment = Legend.LegendHorizontalAlignment.LEFT
        graph.legend.verticalAlignment = Legend.LegendVerticalAlignment.TOP
        graph.legend.setDrawInside(false)
        graph.legend.yOffset = 10f

        when(scelta){
            0->{
                btn1.setOnClickListener {
                    resetChart(graph)
                    var array = arrayListOf<Int>()
                    var nomi = arrayListOf<String>()
                    var i = 0
                    var g = nazione?.count() as Int
                    g-=1
                    for (i in nazione.count()-8..g){
                        array.add(nazione?.elementAt(i).totale_casi)

                    }
                    for (i in nazione.count()-7..g){
                        nomi.add(nazione?.elementAt(i).data.substring(5,10))
                    }
                    val entries: ArrayList<BarEntry> = ArrayList()
                    array = getarray(array)
                    for (i in 0..6){
                        entries.add(BarEntry(i.toFloat(),array[i].toFloat()))
                    }
                    val bardataset = BarDataSet(entries, contesto.getString(R.string.confermatisetti))
                    bardataset.valueTextSize = 10f
                    bardataset.valueTextColor = Color.parseColor("#828282")
                    val data = BarData(bardataset)
                    graph.xAxis.setDrawGridLines(false)
                    graph.axisRight.setDrawGridLines(false)
                    graph.axisLeft.setDrawGridLines(false)
                    graph.setData(data); // set the data and list of lables into chart
                    graph.setScaleEnabled(false)
                    graph.description.text = ""
                    graph.axisRight.xOffset = 10f
                    graph.axisLeft.xOffset = 10f
                    graph.axisLeft.textColor = Color.parseColor("#828282")
                    graph.axisRight.textColor = Color.parseColor("#828282")
                    bardataset.color = Color.parseColor("#E91E62")
                    //graph.xAxis.valueFormatter = IndexAxisValueFormatter(nomi)
                    // graph.xAxis.granularity = 1f
                    // graph.xAxis.isGranularityEnabled = true
                    graph.legend.textColor =  Color.parseColor("#828282")
                    graph.animateY(1000)

                    graph.xAxis.textColor =  Color.parseColor("#828282")
                    graph.xAxis.valueFormatter = (object : ValueFormatter() {
                        val pattern = "dd MMM"
                        private val mFormat = SimpleDateFormat(pattern)
                        private val inputFormat = SimpleDateFormat("MM-dd")
                        override fun getFormattedValue(value: Float): String {
                            val millis = TimeUnit.HOURS.toMillis(value.toLong())
                            return mFormat.format(inputFormat.parse(nomi[value.toInt()]))
                        }
                    })

                }
                btn1.performClick()
                btn2.setOnClickListener {
                    resetChart(graph)
                    var array = arrayListOf<Int>()
                    var nomi = arrayListOf<String>()
                    var i = 0
                    var g = nazione?.count() as Int
                    g-=1
                    for (i in nazione.count()-31..g){
                        array.add(nazione?.elementAt(i).totale_casi)

                    }
                    for (i in nazione.count()-30..g){
                        nomi.add(nazione?.elementAt(i).data.substring(5,10))
                    }
                    val entries: ArrayList<BarEntry> = ArrayList()
                    array = getarray(array)
                    for (i in 0..29){
                        entries.add(BarEntry(i.toFloat(),array[i].toFloat()))
                    }
                    val bardataset = BarDataSet(entries, contesto.getString(R.string.confermatimese))
                    bardataset.valueTextSize = 10f
                    bardataset.valueTextColor = Color.parseColor("#828282")
                    val data = BarData(bardataset)
                    graph.xAxis.setDrawGridLines(false)
                    bardataset.setDrawValues(false)
                    graph.axisRight.setDrawGridLines(false)
                    graph.axisLeft.setDrawGridLines(false)
                    graph.setData(data); // set the data and list of lables into chart
                    graph.setScaleEnabled(false)
                    graph.description.text = ""
                    graph.axisRight.xOffset = 10f
                    graph.axisLeft.xOffset = 10f
                    graph.axisLeft.textColor = Color.parseColor("#828282")
                    graph.axisRight.textColor = Color.parseColor("#828282")
                    bardataset.color = Color.parseColor("#E91E62")
                    //graph.xAxis.valueFormatter = IndexAxisValueFormatter(nomi)
                    // graph.xAxis.granularity = 1f
                    // graph.xAxis.isGranularityEnabled = true
                    graph.legend.textColor =  Color.parseColor("#828282")
                    graph.animateY(1000)

                    graph.xAxis.textColor =  Color.parseColor("#828282")
                    graph.xAxis.valueFormatter = (object : ValueFormatter() {
                        val pattern = "dd MMM"
                        private val mFormat = SimpleDateFormat(pattern)
                        private val inputFormat = SimpleDateFormat("MM-dd")
                        override fun getFormattedValue(value: Float): String {
                            val millis = TimeUnit.HOURS.toMillis(value.toLong())
                            return mFormat.format(inputFormat.parse(nomi[value.toInt()]))
                        }
                    })


                }
                btn3.setOnClickListener {
                    resetChart(graph)
                    var array = arrayListOf<Int>()
                    var nomi = arrayListOf<String>()
                    var i = 0
                    var g = nazione?.count() as Int
                    g-=1
                    for (i in 0..g){
                        array.add(nazione?.elementAt(i).totale_casi)

                    }
                    for (i in 0..g){
                        nomi.add(nazione?.elementAt(i).data.substring(5,10))
                    }
                    val entries: ArrayList<BarEntry> = ArrayList()
                    array = getarray(array)
                    for (i in 0..array.count()-1){
                        entries.add(BarEntry(i.toFloat(),array[i].toFloat()))
                    }
                    val bardataset = BarDataSet(entries, contesto.getString(R.string.confermatianno))
                    bardataset.valueTextSize = 10f
                    bardataset.valueTextColor = Color.parseColor("#828282")
                    val data = BarData(bardataset)
                    graph.xAxis.setDrawGridLines(false)
                    bardataset.setDrawValues(false)
                    graph.axisRight.setDrawGridLines(false)
                    graph.axisLeft.setDrawGridLines(false)
                    graph.setData(data); // set the data and list of lables into chart
                    graph.setScaleEnabled(false)
                    graph.description.text = ""
                    graph.axisRight.xOffset = 10f
                    graph.axisLeft.xOffset = 10f
                    graph.axisLeft.textColor = Color.parseColor("#828282")
                    graph.axisRight.textColor = Color.parseColor("#828282")
                    bardataset.color = Color.parseColor("#E91E62")
                    //graph.xAxis.valueFormatter = IndexAxisValueFormatter(nomi)
                    // graph.xAxis.granularity = 1f
                    // graph.xAxis.isGranularityEnabled = true
                    graph.legend.textColor =  Color.parseColor("#828282")
                    graph.animateY(1000)

                    graph.xAxis.textColor =  Color.parseColor("#828282")
                    graph.xAxis.valueFormatter = (object : ValueFormatter() {
                        val pattern = "dd MMM"
                        private val mFormat = SimpleDateFormat(pattern)
                        private val inputFormat = SimpleDateFormat("MM-dd")
                        override fun getFormattedValue(value: Float): String {
                            val millis = TimeUnit.HOURS.toMillis(value.toLong())
                            return mFormat.format(inputFormat.parse(nomi[value.toInt()]))
                        }
                    })
                }

            }
            1 ->{
                btn1.setOnClickListener {
                    resetChart(graph)
                    var array = arrayListOf<Int>()
                    var nomi = arrayListOf<String>()
                    var i = 0
                    var g = nazione?.count() as Int
                    g-=1
                    for (i in nazione.count()-8..g){
                        array.add(nazione?.elementAt(i).dimessi_guariti)

                    }
                    for (i in nazione.count()-7..g){
                        nomi.add(nazione?.elementAt(i).data.substring(5,10))
                    }
                    val entries: ArrayList<BarEntry> = ArrayList()
                    array = getarray(array)
                    for (i in 0..6){
                        entries.add(BarEntry(i.toFloat(),array[i].toFloat()))
                    }
                    val bardataset = BarDataSet(entries, contesto.getString(R.string.guaritisetti))
                    bardataset.valueTextSize = 10f
                    bardataset.valueTextColor = Color.parseColor("#828282")
                    val data = BarData(bardataset)
                    graph.xAxis.setDrawGridLines(false)
                    graph.axisRight.setDrawGridLines(false)
                    graph.axisLeft.setDrawGridLines(false)
                    graph.setData(data); // set the data and list of lables into chart
                    graph.setScaleEnabled(false)
                    graph.description.text = ""
                    graph.axisRight.xOffset = 10f
                    graph.axisLeft.xOffset = 10f
                    graph.axisLeft.textColor = Color.parseColor("#828282")
                    graph.axisRight.textColor = Color.parseColor("#828282")
                    bardataset.color = Color.parseColor("#E91E62")
                    //graph.xAxis.valueFormatter = IndexAxisValueFormatter(nomi)
                    // graph.xAxis.granularity = 1f
                    // graph.xAxis.isGranularityEnabled = true
                    graph.legend.textColor =  Color.parseColor("#828282")
                    graph.animateY(1000)

                    graph.xAxis.textColor =  Color.parseColor("#828282")
                    graph.xAxis.valueFormatter = (object : ValueFormatter() {
                        val pattern = "dd MMM"
                        private val mFormat = SimpleDateFormat(pattern)
                        private val inputFormat = SimpleDateFormat("MM-dd")
                        override fun getFormattedValue(value: Float): String {
                            val millis = TimeUnit.HOURS.toMillis(value.toLong())
                            return mFormat.format(inputFormat.parse(nomi[value.toInt()]))
                        }
                    })

                }
                btn1.performClick()
                btn2.setOnClickListener {
                    resetChart(graph)
                    var array = arrayListOf<Int>()
                    var nomi = arrayListOf<String>()
                    var i = 0
                    var g = nazione?.count() as Int
                    g-=1
                    for (i in nazione.count()-31..g){
                        array.add(nazione?.elementAt(i).dimessi_guariti)

                    }
                    for (i in nazione.count()-30..g){
                        nomi.add(nazione?.elementAt(i).data.substring(5,10))
                    }
                    val entries: ArrayList<BarEntry> = ArrayList()
                    array = getarray(array)
                    for (i in 0..29){
                        entries.add(BarEntry(i.toFloat(),array[i].toFloat()))
                    }
                    val bardataset = BarDataSet(entries, contesto.getString(R.string.guaritimese))
                    bardataset.valueTextSize = 10f
                    bardataset.valueTextColor = Color.parseColor("#828282")
                    val data = BarData(bardataset)
                    graph.xAxis.setDrawGridLines(false)
                    bardataset.setDrawValues(false)
                    graph.axisRight.setDrawGridLines(false)
                    graph.axisLeft.setDrawGridLines(false)
                    graph.setData(data); // set the data and list of lables into chart
                    graph.setScaleEnabled(false)
                    graph.description.text = ""
                    graph.axisRight.xOffset = 10f
                    graph.axisLeft.xOffset = 10f
                    graph.axisLeft.textColor = Color.parseColor("#828282")
                    graph.axisRight.textColor = Color.parseColor("#828282")
                    bardataset.color = Color.parseColor("#E91E62")
                    //graph.xAxis.valueFormatter = IndexAxisValueFormatter(nomi)
                    // graph.xAxis.granularity = 1f
                    // graph.xAxis.isGranularityEnabled = true
                    graph.legend.textColor =  Color.parseColor("#828282")
                    graph.animateY(1000)

                    graph.xAxis.textColor =  Color.parseColor("#828282")
                    graph.xAxis.valueFormatter = (object : ValueFormatter() {
                        val pattern = "dd MMM"
                        private val mFormat = SimpleDateFormat(pattern)
                        private val inputFormat = SimpleDateFormat("MM-dd")
                        override fun getFormattedValue(value: Float): String {
                            val millis = TimeUnit.HOURS.toMillis(value.toLong())
                            return mFormat.format(inputFormat.parse(nomi[value.toInt()]))
                        }
                    })


                }
                btn3.setOnClickListener {
                    resetChart(graph)
                    var array = arrayListOf<Int>()
                    var nomi = arrayListOf<String>()
                    var i = 0
                    var g = nazione?.count() as Int
                    g-=1
                    for (i in 0..g){
                        array.add(nazione?.elementAt(i).dimessi_guariti)

                    }
                    for (i in 0..g){
                        nomi.add(nazione?.elementAt(i).data.substring(5,10))
                    }
                    val entries: ArrayList<BarEntry> = ArrayList()
                    array = getarray(array)
                    for (i in 0..array.count()-1){
                        entries.add(BarEntry(i.toFloat(),array[i].toFloat()))
                    }
                    val bardataset = BarDataSet(entries, contesto.getString(R.string.guaritianno))
                    bardataset.valueTextSize = 10f
                    bardataset.valueTextColor = Color.parseColor("#828282")
                    val data = BarData(bardataset)
                    graph.xAxis.setDrawGridLines(false)
                    bardataset.setDrawValues(false)
                    graph.axisRight.setDrawGridLines(false)
                    graph.axisLeft.setDrawGridLines(false)
                    graph.setData(data); // set the data and list of lables into chart
                    graph.setScaleEnabled(false)
                    graph.description.text = ""
                    graph.axisRight.xOffset = 10f
                    graph.axisLeft.xOffset = 10f
                    graph.axisLeft.textColor = Color.parseColor("#828282")
                    graph.axisRight.textColor = Color.parseColor("#828282")
                    bardataset.color = Color.parseColor("#E91E62")
                    //graph.xAxis.valueFormatter = IndexAxisValueFormatter(nomi)
                    // graph.xAxis.granularity = 1f
                    // graph.xAxis.isGranularityEnabled = true
                    graph.legend.textColor =  Color.parseColor("#828282")
                    graph.animateY(1000)

                    graph.xAxis.textColor =  Color.parseColor("#828282")
                    graph.xAxis.valueFormatter = (object : ValueFormatter() {
                        val pattern = "dd MMM"
                        private val mFormat = SimpleDateFormat(pattern)
                        private val inputFormat = SimpleDateFormat("MM-dd")
                        override fun getFormattedValue(value: Float): String {
                            val millis = TimeUnit.HOURS.toMillis(value.toLong())
                            return mFormat.format(inputFormat.parse(nomi[value.toInt()]))
                        }
                    })
                }

            }
            2->{
                btn1.setOnClickListener {
                    resetChart(graph)
                    var array = arrayListOf<Int>()
                    var nomi = arrayListOf<String>()
                    var i = 0
                    var g = nazione?.count() as Int
                    g-=1
                    for (i in nazione.count()-8..g){
                        array.add(nazione?.elementAt(i).deceduti)

                    }
                    for (i in nazione.count()-7..g){
                        nomi.add(nazione?.elementAt(i).data.substring(5,10))
                    }
                    val entries: ArrayList<BarEntry> = ArrayList()
                    array = getarray(array)
                    for (i in 0..6){
                        entries.add(BarEntry(i.toFloat(),array[i].toFloat()))
                    }
                    val bardataset = BarDataSet(entries, contesto.getString(R.string.decedutisett))
                    bardataset.valueTextSize = 10f
                    bardataset.valueTextColor = Color.parseColor("#828282")
                    val data = BarData(bardataset)
                    graph.xAxis.setDrawGridLines(false)
                    graph.axisRight.setDrawGridLines(false)
                    graph.axisLeft.setDrawGridLines(false)
                    graph.setData(data); // set the data and list of lables into chart
                    graph.setScaleEnabled(false)
                    graph.description.text = ""
                    graph.axisRight.xOffset = 10f
                    graph.axisLeft.xOffset = 10f
                    graph.axisLeft.textColor = Color.parseColor("#828282")
                    graph.axisRight.textColor = Color.parseColor("#828282")
                    bardataset.color = Color.parseColor("#E91E62")
                    //graph.xAxis.valueFormatter = IndexAxisValueFormatter(nomi)
                    // graph.xAxis.granularity = 1f
                    // graph.xAxis.isGranularityEnabled = true
                    graph.legend.textColor =  Color.parseColor("#828282")
                    graph.animateY(1000)

                    graph.xAxis.textColor =  Color.parseColor("#828282")
                    graph.xAxis.valueFormatter = (object : ValueFormatter() {
                        val pattern = "dd MMM"
                        private val mFormat = SimpleDateFormat(pattern)
                        private val inputFormat = SimpleDateFormat("MM-dd")
                        override fun getFormattedValue(value: Float): String {
                            val millis = TimeUnit.HOURS.toMillis(value.toLong())
                            return mFormat.format(inputFormat.parse(nomi[value.toInt()]))
                        }
                    })

                }
                btn1.performClick()
                btn2.setOnClickListener {
                    resetChart(graph)
                    var array = arrayListOf<Int>()
                    var nomi = arrayListOf<String>()
                    var i = 0
                    var g = nazione?.count() as Int
                    g-=1
                    for (i in nazione.count()-31..g){
                        array.add(nazione?.elementAt(i).deceduti)

                    }
                    for (i in nazione.count()-30..g){
                        nomi.add(nazione?.elementAt(i).data.substring(5,10))
                    }
                    val entries: ArrayList<BarEntry> = ArrayList()
                    array = getarray(array)
                    for (i in 0..29){
                        entries.add(BarEntry(i.toFloat(),array[i].toFloat()))
                    }
                    val bardataset = BarDataSet(entries, contesto.getString(R.string.decedutimese))
                    bardataset.valueTextSize = 10f
                    bardataset.valueTextColor = Color.parseColor("#828282")
                    val data = BarData(bardataset)
                    graph.xAxis.setDrawGridLines(false)
                    bardataset.setDrawValues(false)
                    graph.axisRight.setDrawGridLines(false)
                    graph.axisLeft.setDrawGridLines(false)
                    graph.setData(data); // set the data and list of lables into chart
                    graph.setScaleEnabled(false)
                    graph.description.text = ""
                    graph.axisRight.xOffset = 10f
                    graph.axisLeft.xOffset = 10f
                    graph.axisLeft.textColor = Color.parseColor("#828282")
                    graph.axisRight.textColor = Color.parseColor("#828282")
                    bardataset.color = Color.parseColor("#E91E62")
                    //graph.xAxis.valueFormatter = IndexAxisValueFormatter(nomi)
                    // graph.xAxis.granularity = 1f
                    // graph.xAxis.isGranularityEnabled = true
                    graph.legend.textColor =  Color.parseColor("#828282")
                    graph.animateY(1000)

                    graph.xAxis.textColor =  Color.parseColor("#828282")
                    graph.xAxis.valueFormatter = (object : ValueFormatter() {
                        val pattern = "dd MMM"
                        private val mFormat = SimpleDateFormat(pattern)
                        private val inputFormat = SimpleDateFormat("MM-dd")
                        override fun getFormattedValue(value: Float): String {
                            val millis = TimeUnit.HOURS.toMillis(value.toLong())
                            return mFormat.format(inputFormat.parse(nomi[value.toInt()]))
                        }
                    })


                }
                btn3.setOnClickListener {
                    resetChart(graph)
                    var array = arrayListOf<Int>()
                    var nomi = arrayListOf<String>()
                    var i = 0
                    var g = nazione?.count() as Int
                    g-=1
                    for (i in 0..g){
                        array.add(nazione?.elementAt(i).deceduti)

                    }
                    for (i in 0..g){
                        nomi.add(nazione?.elementAt(i).data.substring(5,10))
                    }
                    val entries: ArrayList<BarEntry> = ArrayList()
                    array = getarray(array)
                    for (i in 0..array.count()-1){
                        entries.add(BarEntry(i.toFloat(),array[i].toFloat()))
                    }
                    val bardataset = BarDataSet(entries, contesto.getString(R.string.decedutianno))
                    bardataset.valueTextSize = 10f
                    bardataset.valueTextColor = Color.parseColor("#828282")
                    val data = BarData(bardataset)
                    graph.xAxis.setDrawGridLines(false)
                    bardataset.setDrawValues(false)
                    graph.axisRight.setDrawGridLines(false)
                    graph.axisLeft.setDrawGridLines(false)
                    graph.setData(data); // set the data and list of lables into chart
                    graph.setScaleEnabled(false)
                    graph.description.text = ""
                    graph.axisRight.xOffset = 10f
                    graph.axisLeft.xOffset = 10f
                    graph.axisLeft.textColor = Color.parseColor("#828282")
                    graph.axisRight.textColor = Color.parseColor("#828282")
                    bardataset.color = Color.parseColor("#E91E62")
                    //graph.xAxis.valueFormatter = IndexAxisValueFormatter(nomi)
                    // graph.xAxis.granularity = 1f
                    // graph.xAxis.isGranularityEnabled = true
                    graph.legend.textColor =  Color.parseColor("#828282")
                    graph.animateY(1000)

                    graph.xAxis.textColor =  Color.parseColor("#828282")
                    graph.xAxis.valueFormatter = (object : ValueFormatter() {
                        val pattern = "dd MMM"
                        private val mFormat = SimpleDateFormat(pattern)
                        private val inputFormat = SimpleDateFormat("MM-dd")
                        override fun getFormattedValue(value: Float): String {
                            val millis = TimeUnit.HOURS.toMillis(value.toLong())
                            return mFormat.format(inputFormat.parse(nomi[value.toInt()]))
                        }
                    })
                }


            }
            else -> "fatti in culo"
        }
    }
    fun getBarChartEuropeoStato(nazione : DatiArray, graph: BarChart, scelta: Int,btn1 : Button,btn2:Button,btn3:Button,contesto : Context,stato : String){
        graph.legend.form = Legend.LegendForm.EMPTY
        graph.axisLeft.isEnabled = false
        graph.xAxis.position =XAxis.XAxisPosition.BOTTOM
        graph.legend.horizontalAlignment = Legend.LegendHorizontalAlignment.LEFT
        graph.legend.verticalAlignment = Legend.LegendVerticalAlignment.TOP
        graph.legend.setDrawInside(false)
        graph.legend.yOffset = 10f

        when(scelta){
            0->{
                btn1.setOnClickListener {
                    resetChart(graph)
                    var array = arrayListOf<Int>()
                    var nomi = arrayListOf<String>()



                    for (i in nazione.confermati[0].totale_casi!!.count()-8..nazione.confermati[0].totale_casi!!.count()-1){

                        nazione.confermati.forEach {
                            if(it.stato == "" && it.Region == stato){
                                array.add(it.totale_casi!!.get(i))
                            }

                        }

                        nomi.add(nazione.giorni.get(i))
                    }



                    val entries: ArrayList<BarEntry> = ArrayList()
                    array = getarray(array)
                    for (i in 0..6){
                        entries.add(BarEntry(i.toFloat(),array[i].toFloat()))
                    }
                    val bardataset = BarDataSet(entries, contesto.getString(R.string.confermatisetti))
                    bardataset.valueTextSize = 10f
                    bardataset.valueTextColor = Color.parseColor("#828282")
                    bardataset.setDrawValues(true)
                    val data = BarData(bardataset)
                    graph.xAxis.setDrawGridLines(false)
                    graph.axisRight.setDrawGridLines(false)
                    graph.axisLeft.setDrawGridLines(false)
                    graph.setData(data); // set the data and list of lables into chart
                    graph.setScaleEnabled(false)
                    graph.description.text = ""
                    graph.axisRight.xOffset = 10f
                    graph.axisLeft.xOffset = 10f
                    graph.axisLeft.textColor = Color.parseColor("#828282")
                    graph.axisRight.textColor = Color.parseColor("#828282")
                    bardataset.color = Color.parseColor("#E91E62")
                    //graph.xAxis.valueFormatter = IndexAxisValueFormatter(nomi)
                    // graph.xAxis.granularity = 1f
                    // graph.xAxis.isGranularityEnabled = true
                    graph.legend.textColor =  Color.parseColor("#828282")
                    graph.animateY(1000)

                    graph.xAxis.textColor =  Color.parseColor("#828282")
                    graph.xAxis.valueFormatter = (object : ValueFormatter() {
                        val pattern = "dd MMM"
                        private val mFormat = SimpleDateFormat(pattern)
                        private val inputFormat = SimpleDateFormat("MM/dd/yy")
                        override fun getFormattedValue(value: Float): String {
                            val millis = TimeUnit.HOURS.toMillis(value.toLong())
                            return mFormat.format(inputFormat.parse(nomi[value.toInt()+1]))
                        }
                    })
                }
                btn1.performClick()
                btn2.setOnClickListener{

                    resetChart(graph)
                    var array = arrayListOf<Int>()
                    var nomi = arrayListOf<String>()



                    for (i in nazione.confermati[0].totale_casi!!.count()-31..nazione.confermati[0].totale_casi!!.count()-1){
                        nazione.confermati.forEach {
                            if(it.stato == "" && it.Region == stato){
                                array.add(it.totale_casi!!.get(i))
                            }

                        }

                        nomi.add(nazione.giorni.get(i))
                    }



                    val entries: ArrayList<BarEntry> = ArrayList()
                    array = getarray(array)
                    for (i in 0..29){
                        entries.add(BarEntry(i.toFloat(),array[i].toFloat()))
                    }
                    val bardataset = BarDataSet(entries, contesto.getString(R.string.confermatimese))
                    bardataset.setDrawValues(false)
                    val data = BarData(bardataset)

                    graph.data = data
                    bardataset.valueTextSize = 10f
                    bardataset.valueTextColor = Color.parseColor("#828282")
                    graph.xAxis.setDrawGridLines(false)
                    graph.axisRight.setDrawGridLines(false)
                    graph.axisLeft.setDrawGridLines(false)

                    ; // set the data and list of lables into chart
                    graph.setScaleEnabled(false)
                    graph.description.text = ""
                    graph.axisRight.xOffset = 10f
                    graph.axisLeft.xOffset = 10f

                    graph.axisLeft.textColor = Color.parseColor("#828282")
                    graph.axisRight.textColor = Color.parseColor("#828282")
                    bardataset.color = Color.parseColor("#E91E62")
                    //graph.xAxis.valueFormatter = IndexAxisValueFormatter(nomi)
                    // graph.xAxis.granularity = 1f
                    // graph.xAxis.isGranularityEnabled = true
                    graph.legend.textColor =  Color.parseColor("#828282")
                    graph.animateY(1000)

                    graph.xAxis.textColor =  Color.parseColor("#828282")
                    graph.xAxis.valueFormatter = (object : ValueFormatter() {
                        val pattern = "dd MMM"
                        private val mFormat = SimpleDateFormat(pattern)
                        private val inputFormat = SimpleDateFormat("MM/dd/yy")
                        override fun getFormattedValue(value: Float): String {
                            val millis = TimeUnit.HOURS.toMillis(value.toLong())
                            return mFormat.format(inputFormat.parse(nomi[value.toInt()]))
                        }
                    })
                }

                btn3.setOnClickListener {
                    resetChart(graph)
                    var array = arrayListOf<Int>()
                    var nomi = arrayListOf<String>()



                    for (i in 0..nazione.confermati[0].totale_casi!!.count()-1){
                        nazione.confermati.forEach {
                            if(it.stato == "" && it.Region == stato){
                                array.add(it.totale_casi!!.get(i))
                            }

                        }

                        nomi.add(nazione.giorni.get(i))
                    }



                    val entries: ArrayList<BarEntry> = ArrayList()
                    array = getarray(array)
                    for (i in 0..nazione.confermati[0].totale_casi!!.count()-2){
                        entries.add(BarEntry(i.toFloat(),array[i].toFloat()))
                    }
                    val bardataset = BarDataSet(entries, contesto.getString(R.string.confermatianno))
                    bardataset.setDrawValues(false)
                    val data = BarData(bardataset)

                    graph.data = data
                    bardataset.valueTextSize = 10f
                    bardataset.valueTextColor = Color.parseColor("#828282")
                    graph.xAxis.setDrawGridLines(false)
                    graph.axisRight.setDrawGridLines(false)
                    graph.axisLeft.setDrawGridLines(false)

                    ; // set the data and list of lables into chart
                    graph.setScaleEnabled(false)
                    graph.description.text = ""
                    graph.axisRight.xOffset = 10f
                    graph.axisLeft.xOffset = 10f

                    graph.axisLeft.textColor = Color.parseColor("#828282")
                    graph.axisRight.textColor = Color.parseColor("#828282")
                    bardataset.color = Color.parseColor("#E91E62")
                    //graph.xAxis.valueFormatter = IndexAxisValueFormatter(nomi)
                    // graph.xAxis.granularity = 1f
                    // graph.xAxis.isGranularityEnabled = true
                    graph.legend.textColor =  Color.parseColor("#828282")
                    graph.animateY(1000)

                    graph.xAxis.textColor =  Color.parseColor("#828282")
                    graph.xAxis.valueFormatter = (object : ValueFormatter() {
                        val pattern = "dd MMM"
                        private val mFormat = SimpleDateFormat(pattern)
                        private val inputFormat = SimpleDateFormat("MM/dd/yy")
                        override fun getFormattedValue(value: Float): String {
                            val millis = TimeUnit.HOURS.toMillis(value.toLong())
                            return mFormat.format(inputFormat.parse(nomi[value.toInt()]))
                        }
                    })
                }
            }


            1 ->{
                btn1.setOnClickListener {
                    resetChart(graph)
                    var array = arrayListOf<Int>()
                    var nomi = arrayListOf<String>()



                    for (i in nazione.guariti[0].totale_casi!!.count()-8..nazione.guariti[0].totale_casi!!.count()-1){
                        nazione.guariti.forEach {
                            if(it.stato == "" && it.Region == stato){
                                array.add(it.totale_casi!!.get(i))
                            }

                        }

                        nomi.add(nazione.giorni.get(i))
                    }



                    val entries: ArrayList<BarEntry> = ArrayList()
                    array = getarray(array)
                    for (i in 0..6){
                        entries.add(BarEntry(i.toFloat(),array[i].toFloat()))
                    }
                    val bardataset = BarDataSet(entries, contesto.getString(R.string.guaritisetti))
                    bardataset.valueTextSize = 10f
                    bardataset.valueTextColor = Color.parseColor("#828282")
                    bardataset.setDrawValues(true)
                    val data = BarData(bardataset)
                    graph.xAxis.setDrawGridLines(false)
                    graph.axisRight.setDrawGridLines(false)
                    graph.axisLeft.setDrawGridLines(false)
                    graph.setData(data); // set the data and list of lables into chart
                    graph.setScaleEnabled(false)
                    graph.description.text = ""
                    graph.axisRight.xOffset = 10f
                    graph.axisLeft.xOffset = 10f
                    graph.axisLeft.textColor = Color.parseColor("#828282")
                    graph.axisRight.textColor = Color.parseColor("#828282")
                    bardataset.color = Color.parseColor("#E91E62")
                    //graph.xAxis.valueFormatter = IndexAxisValueFormatter(nomi)
                    // graph.xAxis.granularity = 1f
                    // graph.xAxis.isGranularityEnabled = true
                    graph.legend.textColor =  Color.parseColor("#828282")
                    graph.animateY(1000)

                    graph.xAxis.textColor =  Color.parseColor("#828282")
                    graph.xAxis.valueFormatter = (object : ValueFormatter() {
                        val pattern = "dd MMM"
                        private val mFormat = SimpleDateFormat(pattern)
                        private val inputFormat = SimpleDateFormat("MM/dd/yy")
                        override fun getFormattedValue(value: Float): String {
                            val millis = TimeUnit.HOURS.toMillis(value.toLong())
                            return mFormat.format(inputFormat.parse(nomi[value.toInt()+1]))
                        }
                    })
                }
                btn1.performClick()
                btn2.setOnClickListener{

                    resetChart(graph)
                    var array = arrayListOf<Int>()
                    var nomi = arrayListOf<String>()



                    for (i in nazione.guariti[0].totale_casi!!.count()-31..nazione.guariti[0].totale_casi!!.count()-1){
                        nazione.guariti.forEach {
                            if(it.stato == "" && it.Region == stato){
                                array.add(it.totale_casi!!.get(i))
                            }

                        }

                        nomi.add(nazione.giorni.get(i))
                    }



                    val entries: ArrayList<BarEntry> = ArrayList()
                    array = getarray(array)
                    for (i in 0..29){
                        entries.add(BarEntry(i.toFloat(),array[i].toFloat()))
                    }
                    val bardataset = BarDataSet(entries, contesto.getString(R.string.guaritimese))
                    bardataset.setDrawValues(false)
                    val data = BarData(bardataset)

                    graph.data = data
                    bardataset.valueTextSize = 10f
                    bardataset.valueTextColor = Color.parseColor("#828282")
                    graph.xAxis.setDrawGridLines(false)
                    graph.axisRight.setDrawGridLines(false)
                    graph.axisLeft.setDrawGridLines(false)

                    ; // set the data and list of lables into chart
                    graph.setScaleEnabled(false)
                    graph.description.text = ""
                    graph.axisRight.xOffset = 10f
                    graph.axisLeft.xOffset = 10f

                    graph.axisLeft.textColor = Color.parseColor("#828282")
                    graph.axisRight.textColor = Color.parseColor("#828282")
                    bardataset.color = Color.parseColor("#E91E62")
                    //graph.xAxis.valueFormatter = IndexAxisValueFormatter(nomi)
                    // graph.xAxis.granularity = 1f
                    // graph.xAxis.isGranularityEnabled = true
                    graph.legend.textColor =  Color.parseColor("#828282")
                    graph.animateY(1000)

                    graph.xAxis.textColor =  Color.parseColor("#828282")
                    graph.xAxis.valueFormatter = (object : ValueFormatter() {
                        val pattern = "dd MMM"
                        private val mFormat = SimpleDateFormat(pattern)
                        private val inputFormat = SimpleDateFormat("MM/dd/yy")
                        override fun getFormattedValue(value: Float): String {
                            val millis = TimeUnit.HOURS.toMillis(value.toLong())
                            return mFormat.format(inputFormat.parse(nomi[value.toInt()]))
                        }
                    })
                }

                btn3.setOnClickListener {
                    resetChart(graph)
                    var array = arrayListOf<Int>()
                    var nomi = arrayListOf<String>()



                    for (i in 0..nazione.guariti[0].totale_casi!!.count()-1){
                        nazione.guariti.forEach {
                            if(it.stato == "" && it.Region == stato){
                                array.add(it.totale_casi!!.get(i))
                            }

                        }

                        nomi.add(nazione.giorni.get(i))
                    }



                    val entries: ArrayList<BarEntry> = ArrayList()
                    array = getarray(array)
                    for (i in 0..nazione.guariti[0].totale_casi!!.count()-2){
                        entries.add(BarEntry(i.toFloat(),array[i].toFloat()))
                    }
                    val bardataset = BarDataSet(entries, contesto.getString(R.string.guaritianno))
                    bardataset.setDrawValues(false)
                    val data = BarData(bardataset)

                    graph.data = data
                    bardataset.valueTextSize = 10f
                    bardataset.valueTextColor = Color.parseColor("#828282")
                    graph.xAxis.setDrawGridLines(false)
                    graph.axisRight.setDrawGridLines(false)
                    graph.axisLeft.setDrawGridLines(false)

                    ; // set the data and list of lables into chart
                    graph.setScaleEnabled(false)
                    graph.description.text = ""
                    graph.axisRight.xOffset = 10f
                    graph.axisLeft.xOffset = 10f

                    graph.axisLeft.textColor = Color.parseColor("#828282")
                    graph.axisRight.textColor = Color.parseColor("#828282")
                    bardataset.color = Color.parseColor("#E91E62")
                    //graph.xAxis.valueFormatter = IndexAxisValueFormatter(nomi)
                    // graph.xAxis.granularity = 1f
                    // graph.xAxis.isGranularityEnabled = true
                    graph.legend.textColor =  Color.parseColor("#828282")
                    graph.animateY(1000)

                    graph.xAxis.textColor =  Color.parseColor("#828282")
                    graph.xAxis.valueFormatter = (object : ValueFormatter() {
                        val pattern = "dd MMM"
                        private val mFormat = SimpleDateFormat(pattern)
                        private val inputFormat = SimpleDateFormat("MM/dd/yy")
                        override fun getFormattedValue(value: Float): String {
                            val millis = TimeUnit.HOURS.toMillis(value.toLong())
                            return mFormat.format(inputFormat.parse(nomi[value.toInt()]))
                        }
                    })
                }

            }
            2->{
                btn1.setOnClickListener {
                    resetChart(graph)
                    var array = arrayListOf<Int>()
                    var nomi = arrayListOf<String>()



                    for (i in nazione.deceduti[0].totale_casi!!.count()-8..nazione.deceduti[0].totale_casi!!.count()-1){
                        nazione.deceduti.forEach {
                            if(it.stato == "" && it.Region == stato){
                                array.add(it.totale_casi!!.get(i))
                            }

                        }

                        nomi.add(nazione.giorni.get(i))
                    }



                    val entries: ArrayList<BarEntry> = ArrayList()
                    array = getarray(array)
                    for (i in 0..6){
                        entries.add(BarEntry(i.toFloat(),array[i].toFloat()))
                    }
                    val bardataset = BarDataSet(entries, contesto.getString(R.string.decedutisett))
                    bardataset.valueTextSize = 10f
                    bardataset.valueTextColor = Color.parseColor("#828282")
                    bardataset.setDrawValues(true)
                    val data = BarData(bardataset)
                    graph.xAxis.setDrawGridLines(false)
                    graph.axisRight.setDrawGridLines(false)
                    graph.axisLeft.setDrawGridLines(false)
                    graph.setData(data); // set the data and list of lables into chart
                    graph.setScaleEnabled(false)
                    graph.description.text = ""
                    graph.axisRight.xOffset = 10f
                    graph.axisLeft.xOffset = 10f
                    graph.axisLeft.textColor = Color.parseColor("#828282")
                    graph.axisRight.textColor = Color.parseColor("#828282")
                    bardataset.color = Color.parseColor("#E91E62")
                    //graph.xAxis.valueFormatter = IndexAxisValueFormatter(nomi)
                    // graph.xAxis.granularity = 1f
                    // graph.xAxis.isGranularityEnabled = true
                    graph.legend.textColor =  Color.parseColor("#828282")
                    graph.animateY(1000)

                    graph.xAxis.textColor =  Color.parseColor("#828282")
                    graph.xAxis.valueFormatter = (object : ValueFormatter() {
                        val pattern = "dd MMM"
                        private val mFormat = SimpleDateFormat(pattern)
                        private val inputFormat = SimpleDateFormat("MM/dd/yy")
                        override fun getFormattedValue(value: Float): String {
                            val millis = TimeUnit.HOURS.toMillis(value.toLong())
                            return mFormat.format(inputFormat.parse(nomi[value.toInt()+1]))
                        }
                    })
                }
                btn1.performClick()
                btn2.setOnClickListener{

                    resetChart(graph)
                    var array = arrayListOf<Int>()
                    var nomi = arrayListOf<String>()



                    for (i in nazione.deceduti[0].totale_casi!!.count()-31..nazione.deceduti[0].totale_casi!!.count()-1){
                        nazione.deceduti.forEach {
                            if(it.stato == "" && it.Region == stato){
                                array.add(it.totale_casi!!.get(i))
                            }

                        }

                        nomi.add(nazione.giorni.get(i))
                    }



                    val entries: ArrayList<BarEntry> = ArrayList()
                    array = getarray(array)
                    for (i in 0..29){
                        entries.add(BarEntry(i.toFloat(),array[i].toFloat()))
                    }
                    val bardataset = BarDataSet(entries, contesto.getString(R.string.decedutimese))
                    bardataset.setDrawValues(false)
                    val data = BarData(bardataset)

                    graph.data = data
                    bardataset.valueTextSize = 10f
                    bardataset.valueTextColor = Color.parseColor("#828282")
                    graph.xAxis.setDrawGridLines(false)
                    graph.axisRight.setDrawGridLines(false)
                    graph.axisLeft.setDrawGridLines(false)

                    ; // set the data and list of lables into chart
                    graph.setScaleEnabled(false)
                    graph.description.text = ""
                    graph.axisRight.xOffset = 10f
                    graph.axisLeft.xOffset = 10f

                    graph.axisLeft.textColor = Color.parseColor("#828282")
                    graph.axisRight.textColor = Color.parseColor("#828282")
                    bardataset.color = Color.parseColor("#E91E62")
                    //graph.xAxis.valueFormatter = IndexAxisValueFormatter(nomi)
                    // graph.xAxis.granularity = 1f
                    // graph.xAxis.isGranularityEnabled = true
                    graph.legend.textColor =  Color.parseColor("#828282")
                    graph.animateY(1000)

                    graph.xAxis.textColor =  Color.parseColor("#828282")
                    graph.xAxis.valueFormatter = (object : ValueFormatter() {
                        val pattern = "dd MMM"
                        private val mFormat = SimpleDateFormat(pattern)
                        private val inputFormat = SimpleDateFormat("MM/dd/yy")
                        override fun getFormattedValue(value: Float): String {
                            val millis = TimeUnit.HOURS.toMillis(value.toLong())
                            return mFormat.format(inputFormat.parse(nomi[value.toInt()]))
                        }
                    })
                }

                btn3.setOnClickListener {
                    resetChart(graph)
                    var array = arrayListOf<Int>()
                    var nomi = arrayListOf<String>()



                    for (i in 0..nazione.deceduti[0].totale_casi!!.count()-1){
                        nazione.deceduti.forEach {
                            if(it.stato == "" && it.Region == stato){
                                array.add(it.totale_casi!!.get(i))
                            }

                        }

                        nomi.add(nazione.giorni.get(i))
                    }



                    val entries: ArrayList<BarEntry> = ArrayList()
                    array = getarray(array)
                    for (i in 0..nazione.deceduti[0].totale_casi!!.count()-2){
                        entries.add(BarEntry(i.toFloat(),array[i].toFloat()))
                    }
                    val bardataset = BarDataSet(entries, contesto.getString(R.string.decedutianno))
                    bardataset.setDrawValues(false)
                    val data = BarData(bardataset)

                    graph.data = data
                    bardataset.valueTextSize = 10f
                    bardataset.valueTextColor = Color.parseColor("#828282")
                    graph.xAxis.setDrawGridLines(false)
                    graph.axisRight.setDrawGridLines(false)
                    graph.axisLeft.setDrawGridLines(false)

                    ; // set the data and list of lables into chart
                    graph.setScaleEnabled(false)
                    graph.description.text = ""
                    graph.axisRight.xOffset = 10f
                    graph.axisLeft.xOffset = 10f

                    graph.axisLeft.textColor = Color.parseColor("#828282")
                    graph.axisRight.textColor = Color.parseColor("#828282")
                    bardataset.color = Color.parseColor("#E91E62")
                    //graph.xAxis.valueFormatter = IndexAxisValueFormatter(nomi)
                    // graph.xAxis.granularity = 1f
                    // graph.xAxis.isGranularityEnabled = true
                    graph.legend.textColor =  Color.parseColor("#828282")
                    graph.animateY(1000)

                    graph.xAxis.textColor =  Color.parseColor("#828282")
                    graph.xAxis.valueFormatter = (object : ValueFormatter() {
                        val pattern = "dd MMM"
                        private val mFormat = SimpleDateFormat(pattern)
                        private val inputFormat = SimpleDateFormat("MM/dd/yy")
                        override fun getFormattedValue(value: Float): String {
                            val millis = TimeUnit.HOURS.toMillis(value.toLong())
                            return mFormat.format(inputFormat.parse(nomi[value.toInt()]))
                        }
                    })
                }


            }
            else -> "fatti in culo"
        }
    }



    fun getBarChartEuropeo(nazione : DatiArray, graph: BarChart, scelta: Int,btn1 : Button,btn2:Button,btn3:Button,contesto : Context){
        graph.legend.form = Legend.LegendForm.EMPTY
        graph.axisLeft.isEnabled = false
        graph.xAxis.position =XAxis.XAxisPosition.BOTTOM
        graph.legend.horizontalAlignment = Legend.LegendHorizontalAlignment.LEFT
        graph.legend.verticalAlignment = Legend.LegendVerticalAlignment.TOP
        graph.legend.setDrawInside(false)
        graph.legend.yOffset = 10f

        when(scelta){
            0->{
                btn1.setOnClickListener {
                    resetChart(graph)
                    var array = arrayListOf<Int>()
                    var nomi = arrayListOf<String>()



                    for (i in nazione.confermati[0].totale_casi!!.count()-8..nazione.confermati[0].totale_casi!!.count()-1){
                        var somma = 0
                        nazione.confermati.forEach {
                            somma+= it.totale_casi!!.get(i)
                        }
                        array.add(somma)
                        nomi.add(nazione.giorni.get(i))
                    }



                    val entries: ArrayList<BarEntry> = ArrayList()
                    array = getarray(array)
                    for (i in 0..6){
                        entries.add(BarEntry(i.toFloat(),array[i].toFloat()))
                    }
                    val bardataset = BarDataSet(entries, contesto.getString(R.string.confermatisetti))
                    bardataset.valueTextSize = 10f
                    bardataset.valueTextColor = Color.parseColor("#828282")
                    bardataset.setDrawValues(true)
                    val data = BarData(bardataset)
                    graph.xAxis.setDrawGridLines(false)
                    graph.axisRight.setDrawGridLines(false)
                    graph.axisLeft.setDrawGridLines(false)
                    graph.setData(data); // set the data and list of lables into chart
                    graph.setScaleEnabled(false)
                    graph.description.text = ""
                    graph.axisRight.xOffset = 10f
                    graph.axisLeft.xOffset = 10f
                    graph.axisLeft.textColor = Color.parseColor("#828282")
                    graph.axisRight.textColor = Color.parseColor("#828282")
                    bardataset.color = Color.parseColor("#E91E62")
                    //graph.xAxis.valueFormatter = IndexAxisValueFormatter(nomi)
                    // graph.xAxis.granularity = 1f
                    // graph.xAxis.isGranularityEnabled = true
                    graph.legend.textColor =  Color.parseColor("#828282")
                    graph.animateY(1000)

                    graph.xAxis.textColor =  Color.parseColor("#828282")
                    graph.xAxis.valueFormatter = (object : ValueFormatter() {
                        val pattern = "dd MMM"
                        private val mFormat = SimpleDateFormat(pattern)
                        private val inputFormat = SimpleDateFormat("MM/dd/yy")
                        override fun getFormattedValue(value: Float): String {
                            val millis = TimeUnit.HOURS.toMillis(value.toLong())
                            return mFormat.format(inputFormat.parse(nomi[value.toInt()+1]))
                        }
                    })
                }
                btn1.performClick()
                btn2.setOnClickListener{

                    resetChart(graph)
                    var array = arrayListOf<Int>()
                    var nomi = arrayListOf<String>()



                    for (i in nazione.confermati[0].totale_casi!!.count()-31..nazione.confermati[0].totale_casi!!.count()-1){
                        var somma = 0
                        nazione.confermati.forEach {
                            somma+= it.totale_casi!!.get(i)
                        }
                        array.add(somma)
                        nomi.add(nazione.giorni.get(i))
                    }



                    val entries: ArrayList<BarEntry> = ArrayList()
                    array = getarray(array)
                    for (i in 0..29){
                        entries.add(BarEntry(i.toFloat(),array[i].toFloat()))
                    }
                    val bardataset = BarDataSet(entries, contesto.getString(R.string.confermatimese))
                    bardataset.setDrawValues(false)
                    val data = BarData(bardataset)

                    graph.data = data
                    bardataset.valueTextSize = 10f
                    bardataset.valueTextColor = Color.parseColor("#828282")
                    graph.xAxis.setDrawGridLines(false)
                    graph.axisRight.setDrawGridLines(false)
                    graph.axisLeft.setDrawGridLines(false)

                    ; // set the data and list of lables into chart
                    graph.setScaleEnabled(false)
                    graph.description.text = ""
                    graph.axisRight.xOffset = 10f
                    graph.axisLeft.xOffset = 10f

                    graph.axisLeft.textColor = Color.parseColor("#828282")
                    graph.axisRight.textColor = Color.parseColor("#828282")
                    bardataset.color = Color.parseColor("#E91E62")
                    //graph.xAxis.valueFormatter = IndexAxisValueFormatter(nomi)
                    // graph.xAxis.granularity = 1f
                    // graph.xAxis.isGranularityEnabled = true
                    graph.legend.textColor =  Color.parseColor("#828282")
                    graph.animateY(1000)

                    graph.xAxis.textColor =  Color.parseColor("#828282")
                    graph.xAxis.valueFormatter = (object : ValueFormatter() {
                        val pattern = "dd MMM"
                        private val mFormat = SimpleDateFormat(pattern)
                        private val inputFormat = SimpleDateFormat("MM/dd/yy")
                        override fun getFormattedValue(value: Float): String {
                            val millis = TimeUnit.HOURS.toMillis(value.toLong())
                            return mFormat.format(inputFormat.parse(nomi[value.toInt()]))
                        }
                    })
                }

                btn3.setOnClickListener {
                    resetChart(graph)
                    var array = arrayListOf<Int>()
                    var nomi = arrayListOf<String>()



                    for (i in 0..nazione.confermati[0].totale_casi!!.count()-1){
                        var somma = 0
                        nazione.confermati.forEach {
                            somma+= it.totale_casi!!.get(i)
                        }
                        array.add(somma)
                        nomi.add(nazione.giorni.get(i))
                    }



                    val entries: ArrayList<BarEntry> = ArrayList()
                    array = getarray(array)
                    for (i in 0..nazione.confermati[0].totale_casi!!.count()-2){
                        entries.add(BarEntry(i.toFloat(),array[i].toFloat()))
                    }
                    val bardataset = BarDataSet(entries, contesto.getString(R.string.confermatianno))
                    bardataset.setDrawValues(false)
                    val data = BarData(bardataset)

                    graph.data = data
                    bardataset.valueTextSize = 10f
                    bardataset.valueTextColor = Color.parseColor("#828282")
                    graph.xAxis.setDrawGridLines(false)
                    graph.axisRight.setDrawGridLines(false)
                    graph.axisLeft.setDrawGridLines(false)

                    ; // set the data and list of lables into chart
                    graph.setScaleEnabled(false)
                    graph.description.text = ""
                    graph.axisRight.xOffset = 10f
                    graph.axisLeft.xOffset = 10f

                    graph.axisLeft.textColor = Color.parseColor("#828282")
                    graph.axisRight.textColor = Color.parseColor("#828282")
                    bardataset.color = Color.parseColor("#E91E62")
                    //graph.xAxis.valueFormatter = IndexAxisValueFormatter(nomi)
                    // graph.xAxis.granularity = 1f
                    // graph.xAxis.isGranularityEnabled = true
                    graph.legend.textColor =  Color.parseColor("#828282")
                    graph.animateY(1000)

                    graph.xAxis.textColor =  Color.parseColor("#828282")
                    graph.xAxis.valueFormatter = (object : ValueFormatter() {
                        val pattern = "dd MMM"
                        private val mFormat = SimpleDateFormat(pattern)
                        private val inputFormat = SimpleDateFormat("MM/dd/yy")
                        override fun getFormattedValue(value: Float): String {
                            val millis = TimeUnit.HOURS.toMillis(value.toLong())
                            return mFormat.format(inputFormat.parse(nomi[value.toInt()]))
                        }
                    })
                }
                }


            1 ->{
                btn1.setOnClickListener {
                    resetChart(graph)
                    var array = arrayListOf<Int>()
                    var nomi = arrayListOf<String>()



                    for (i in nazione.guariti[0].totale_casi!!.count()-8..nazione.guariti[0].totale_casi!!.count()-1){
                        var somma = 0
                        nazione.guariti.forEach {
                            somma+= it.totale_casi!!.get(i)
                        }
                        array.add(somma)
                        nomi.add(nazione.giorni.get(i))
                    }



                    val entries: ArrayList<BarEntry> = ArrayList()
                    array = getarray(array)
                    for (i in 0..6){
                        entries.add(BarEntry(i.toFloat(),array[i].toFloat()))
                    }
                    val bardataset = BarDataSet(entries, contesto.getString(R.string.guaritisetti))
                    bardataset.valueTextSize = 10f
                    bardataset.valueTextColor = Color.parseColor("#828282")
                    bardataset.setDrawValues(true)
                    val data = BarData(bardataset)
                    graph.xAxis.setDrawGridLines(false)
                    graph.axisRight.setDrawGridLines(false)
                    graph.axisLeft.setDrawGridLines(false)
                    graph.setData(data); // set the data and list of lables into chart
                    graph.setScaleEnabled(false)
                    graph.description.text = ""
                    graph.axisRight.xOffset = 10f
                    graph.axisLeft.xOffset = 10f
                    graph.axisLeft.textColor = Color.parseColor("#828282")
                    graph.axisRight.textColor = Color.parseColor("#828282")
                    bardataset.color = Color.parseColor("#E91E62")
                    //graph.xAxis.valueFormatter = IndexAxisValueFormatter(nomi)
                    // graph.xAxis.granularity = 1f
                    // graph.xAxis.isGranularityEnabled = true
                    graph.legend.textColor =  Color.parseColor("#828282")
                    graph.animateY(1000)

                    graph.xAxis.textColor =  Color.parseColor("#828282")
                    graph.xAxis.valueFormatter = (object : ValueFormatter() {
                        val pattern = "dd MMM"
                        private val mFormat = SimpleDateFormat(pattern)
                        private val inputFormat = SimpleDateFormat("MM/dd/yy")
                        override fun getFormattedValue(value: Float): String {
                            val millis = TimeUnit.HOURS.toMillis(value.toLong())
                            return mFormat.format(inputFormat.parse(nomi[value.toInt()+1]))
                        }
                    })
                }
                btn1.performClick()
                btn2.setOnClickListener{

                    resetChart(graph)
                    var array = arrayListOf<Int>()
                    var nomi = arrayListOf<String>()



                    for (i in nazione.guariti[0].totale_casi!!.count()-31..nazione.guariti[0].totale_casi!!.count()-1){
                        var somma = 0
                        nazione.guariti.forEach {
                            somma+= it.totale_casi!!.get(i)
                        }
                        array.add(somma)
                        nomi.add(nazione.giorni.get(i))
                    }



                    val entries: ArrayList<BarEntry> = ArrayList()
                    array = getarray(array)
                    for (i in 0..29){
                        entries.add(BarEntry(i.toFloat(),array[i].toFloat()))
                    }
                    val bardataset = BarDataSet(entries, contesto.getString(R.string.guaritimese))
                    bardataset.setDrawValues(false)
                    val data = BarData(bardataset)

                    graph.data = data
                    bardataset.valueTextSize = 10f
                    bardataset.valueTextColor = Color.parseColor("#828282")
                    graph.xAxis.setDrawGridLines(false)
                    graph.axisRight.setDrawGridLines(false)
                    graph.axisLeft.setDrawGridLines(false)

                    ; // set the data and list of lables into chart
                    graph.setScaleEnabled(false)
                    graph.description.text = ""
                    graph.axisRight.xOffset = 10f
                    graph.axisLeft.xOffset = 10f

                    graph.axisLeft.textColor = Color.parseColor("#828282")
                    graph.axisRight.textColor = Color.parseColor("#828282")
                    bardataset.color = Color.parseColor("#E91E62")
                    //graph.xAxis.valueFormatter = IndexAxisValueFormatter(nomi)
                    // graph.xAxis.granularity = 1f
                    // graph.xAxis.isGranularityEnabled = true
                    graph.legend.textColor =  Color.parseColor("#828282")
                    graph.animateY(1000)

                    graph.xAxis.textColor =  Color.parseColor("#828282")
                    graph.xAxis.valueFormatter = (object : ValueFormatter() {
                        val pattern = "dd MMM"
                        private val mFormat = SimpleDateFormat(pattern)
                        private val inputFormat = SimpleDateFormat("MM/dd/yy")
                        override fun getFormattedValue(value: Float): String {
                            val millis = TimeUnit.HOURS.toMillis(value.toLong())
                            return mFormat.format(inputFormat.parse(nomi[value.toInt()]))
                        }
                    })
                }

                btn3.setOnClickListener {
                    resetChart(graph)
                    var array = arrayListOf<Int>()
                    var nomi = arrayListOf<String>()



                    for (i in 0..nazione.guariti[0].totale_casi!!.count()-1){
                        var somma = 0
                        nazione.guariti.forEach {
                            somma+= it.totale_casi!!.get(i)
                        }
                        array.add(somma)
                        nomi.add(nazione.giorni.get(i))
                    }



                    val entries: ArrayList<BarEntry> = ArrayList()
                    array = getarray(array)
                    for (i in 0..nazione.guariti[0].totale_casi!!.count()-2){
                        entries.add(BarEntry(i.toFloat(),array[i].toFloat()))
                    }
                    val bardataset = BarDataSet(entries, contesto.getString(R.string.guaritianno))
                    bardataset.setDrawValues(false)
                    val data = BarData(bardataset)

                    graph.data = data
                    bardataset.valueTextSize = 10f
                    bardataset.valueTextColor = Color.parseColor("#828282")
                    graph.xAxis.setDrawGridLines(false)
                    graph.axisRight.setDrawGridLines(false)
                    graph.axisLeft.setDrawGridLines(false)

                    ; // set the data and list of lables into chart
                    graph.setScaleEnabled(false)
                    graph.description.text = ""
                    graph.axisRight.xOffset = 10f
                    graph.axisLeft.xOffset = 10f

                    graph.axisLeft.textColor = Color.parseColor("#828282")
                    graph.axisRight.textColor = Color.parseColor("#828282")
                    bardataset.color = Color.parseColor("#E91E62")
                    //graph.xAxis.valueFormatter = IndexAxisValueFormatter(nomi)
                    // graph.xAxis.granularity = 1f
                    // graph.xAxis.isGranularityEnabled = true
                    graph.legend.textColor =  Color.parseColor("#828282")
                    graph.animateY(1000)

                    graph.xAxis.textColor =  Color.parseColor("#828282")
                    graph.xAxis.valueFormatter = (object : ValueFormatter() {
                        val pattern = "dd MMM"
                        private val mFormat = SimpleDateFormat(pattern)
                        private val inputFormat = SimpleDateFormat("MM/dd/yy")
                        override fun getFormattedValue(value: Float): String {
                            val millis = TimeUnit.HOURS.toMillis(value.toLong())
                            return mFormat.format(inputFormat.parse(nomi[value.toInt()]))
                        }
                    })
                }

            }
            2->{
                btn1.setOnClickListener {
                    resetChart(graph)
                    var array = arrayListOf<Int>()
                    var nomi = arrayListOf<String>()



                    for (i in nazione.deceduti[0].totale_casi!!.count()-8..nazione.deceduti[0].totale_casi!!.count()-1){
                        var somma = 0
                        nazione.deceduti.forEach {
                            somma+= it.totale_casi!!.get(i)
                        }
                        array.add(somma)
                        nomi.add(nazione.giorni.get(i))
                    }



                    val entries: ArrayList<BarEntry> = ArrayList()
                    array = getarray(array)
                    for (i in 0..6){
                        entries.add(BarEntry(i.toFloat(),array[i].toFloat()))
                    }
                    val bardataset = BarDataSet(entries, contesto.getString(R.string.decedutisett))
                    bardataset.valueTextSize = 10f
                    bardataset.valueTextColor = Color.parseColor("#828282")
                    bardataset.setDrawValues(true)
                    val data = BarData(bardataset)
                    graph.xAxis.setDrawGridLines(false)
                    graph.axisRight.setDrawGridLines(false)
                    graph.axisLeft.setDrawGridLines(false)
                    graph.setData(data); // set the data and list of lables into chart
                    graph.setScaleEnabled(false)
                    graph.description.text = ""
                    graph.axisRight.xOffset = 10f
                    graph.axisLeft.xOffset = 10f
                    graph.axisLeft.textColor = Color.parseColor("#828282")
                    graph.axisRight.textColor = Color.parseColor("#828282")
                    bardataset.color = Color.parseColor("#E91E62")
                    //graph.xAxis.valueFormatter = IndexAxisValueFormatter(nomi)
                    // graph.xAxis.granularity = 1f
                    // graph.xAxis.isGranularityEnabled = true
                    graph.legend.textColor =  Color.parseColor("#828282")
                    graph.animateY(1000)

                    graph.xAxis.textColor =  Color.parseColor("#828282")
                    graph.xAxis.valueFormatter = (object : ValueFormatter() {
                        val pattern = "dd MMM"
                        private val mFormat = SimpleDateFormat(pattern)
                        private val inputFormat = SimpleDateFormat("MM/dd/yy")
                        override fun getFormattedValue(value: Float): String {
                            val millis = TimeUnit.HOURS.toMillis(value.toLong())
                            return mFormat.format(inputFormat.parse(nomi[value.toInt()+1]))
                        }
                    })
                }
                btn1.performClick()
                btn2.setOnClickListener{

                    resetChart(graph)
                    var array = arrayListOf<Int>()
                    var nomi = arrayListOf<String>()



                    for (i in nazione.deceduti[0].totale_casi!!.count()-31..nazione.deceduti[0].totale_casi!!.count()-1){
                        var somma = 0
                        nazione.deceduti.forEach {
                            somma+= it.totale_casi!!.get(i)
                        }
                        array.add(somma)
                        nomi.add(nazione.giorni.get(i))
                    }



                    val entries: ArrayList<BarEntry> = ArrayList()
                    array = getarray(array)
                    for (i in 0..29){
                        entries.add(BarEntry(i.toFloat(),array[i].toFloat()))
                    }
                    val bardataset = BarDataSet(entries, contesto.getString(R.string.decedutimese))
                    bardataset.setDrawValues(false)
                    val data = BarData(bardataset)

                    graph.data = data
                    bardataset.valueTextSize = 10f
                    bardataset.valueTextColor = Color.parseColor("#828282")
                    graph.xAxis.setDrawGridLines(false)
                    graph.axisRight.setDrawGridLines(false)
                    graph.axisLeft.setDrawGridLines(false)

                    ; // set the data and list of lables into chart
                    graph.setScaleEnabled(false)
                    graph.description.text = ""
                    graph.axisRight.xOffset = 10f
                    graph.axisLeft.xOffset = 10f

                    graph.axisLeft.textColor = Color.parseColor("#828282")
                    graph.axisRight.textColor = Color.parseColor("#828282")
                    bardataset.color = Color.parseColor("#E91E62")
                    //graph.xAxis.valueFormatter = IndexAxisValueFormatter(nomi)
                    // graph.xAxis.granularity = 1f
                    // graph.xAxis.isGranularityEnabled = true
                    graph.legend.textColor =  Color.parseColor("#828282")
                    graph.animateY(1000)

                    graph.xAxis.textColor =  Color.parseColor("#828282")
                    graph.xAxis.valueFormatter = (object : ValueFormatter() {
                        val pattern = "dd MMM"
                        private val mFormat = SimpleDateFormat(pattern)
                        private val inputFormat = SimpleDateFormat("MM/dd/yy")
                        override fun getFormattedValue(value: Float): String {
                            val millis = TimeUnit.HOURS.toMillis(value.toLong())
                            return mFormat.format(inputFormat.parse(nomi[value.toInt()]))
                        }
                    })
                }

                btn3.setOnClickListener {
                    resetChart(graph)
                    var array = arrayListOf<Int>()
                    var nomi = arrayListOf<String>()



                    for (i in 0..nazione.deceduti[0].totale_casi!!.count()-1){
                        var somma = 0
                        nazione.deceduti.forEach {
                            somma+= it.totale_casi!!.get(i)
                        }
                        array.add(somma)
                        nomi.add(nazione.giorni.get(i))
                    }



                    val entries: ArrayList<BarEntry> = ArrayList()
                    array = getarray(array)
                    for (i in 0..nazione.deceduti[0].totale_casi!!.count()-2){
                        entries.add(BarEntry(i.toFloat(),array[i].toFloat()))
                    }
                    val bardataset = BarDataSet(entries, contesto.getString(R.string.decedutianno))
                    bardataset.setDrawValues(false)
                    val data = BarData(bardataset)

                    graph.data = data
                    bardataset.valueTextSize = 10f
                    bardataset.valueTextColor = Color.parseColor("#828282")
                    graph.xAxis.setDrawGridLines(false)
                    graph.axisRight.setDrawGridLines(false)
                    graph.axisLeft.setDrawGridLines(false)

                    ; // set the data and list of lables into chart
                    graph.setScaleEnabled(false)
                    graph.description.text = ""
                    graph.axisRight.xOffset = 10f
                    graph.axisLeft.xOffset = 10f

                    graph.axisLeft.textColor = Color.parseColor("#828282")
                    graph.axisRight.textColor = Color.parseColor("#828282")
                    bardataset.color = Color.parseColor("#E91E62")
                    //graph.xAxis.valueFormatter = IndexAxisValueFormatter(nomi)
                    // graph.xAxis.granularity = 1f
                    // graph.xAxis.isGranularityEnabled = true
                    graph.legend.textColor =  Color.parseColor("#828282")
                    graph.animateY(1000)

                    graph.xAxis.textColor =  Color.parseColor("#828282")
                    graph.xAxis.valueFormatter = (object : ValueFormatter() {
                        val pattern = "dd MMM"
                        private val mFormat = SimpleDateFormat(pattern)
                        private val inputFormat = SimpleDateFormat("MM/dd/yy")
                        override fun getFormattedValue(value: Float): String {
                            val millis = TimeUnit.HOURS.toMillis(value.toLong())
                            return mFormat.format(inputFormat.parse(nomi[value.toInt()]))
                        }
                    })
                }


            }
            else -> "fatti in culo"
        }
    }
    private fun resetChart(barChart : BarChart) {
        barChart.fitScreen()
        barChart.data?.clearValues()
        barChart.xAxis.valueFormatter = null
        barChart.notifyDataSetChanged()
        barChart.clear()
        barChart.invalidate()
    }
    fun withSuffix(count: Long): String? {
        if (count < 1000) return "" + count
        val exp = (Math.log(count.toDouble()) / Math.log(1000.0)).toInt()
        return String.format(
            "%.1f %c",
            count / Math.pow(1000.0, exp.toDouble()),
            "kMGTPE"[exp - 1]
        )
    }
    fun getarray(totali : ArrayList<Int>) : ArrayList<Int>{
        val numeri = arrayListOf<Int>()
        for (i in 0..totali.count()-2){
            var appoggio = totali[i].minus(totali[i+1])
            numeri.add(abs(appoggio))
        }
        return numeri
    }
    fun getLineeChart(nazione : Collection<DatiNazionali>?, graph : LineChart){
        val lista = arrayListOf<Entry>()
        val lista1 = arrayListOf<Entry>()
        val lista2 = arrayListOf<Entry>()
        var cont = 0
        graph.animateX(1000)
        var xAxisLabel = ArrayList<String>(graph.size)
        nazione?.forEach {
            val giorno = it.data.subSequence(5,10).toString()
            val quantita = it.totale_casi.toFloat()
            val deceduti = it.deceduti.toFloat()
            val guariti = it.dimessi_guariti.toFloat()
            xAxisLabel.add(giorno)
            lista.add(Entry(cont.toFloat(),quantita))
            lista1.add(Entry(cont.toFloat(),guariti))
            lista2.add(Entry(cont.toFloat(),deceduti))
            cont+=1
        }


        graph.setOnChartValueSelectedListener(this)

        val lineDataset = LineDataSet(lista,"Confermati")
        val lineDataset1 = LineDataSet(lista1,"Guariti")
        val lineDataset2 = LineDataSet(lista2,"Deceduti")

        graph.xAxis.position =XAxis.XAxisPosition.BOTTOM

        val data = LineData(lineDataset,lineDataset1,lineDataset2)
        setLine(lineDataset, "#5E6D92")
        setLine(lineDataset1, "#F44336")
        setLine(lineDataset, "#6BB564")
        graph.data = data
        graph.setScaleEnabled(false)
        // graph.viewPortHandler.setMaximumScaleX(2f)

        graph.viewPortHandler.setMaximumScaleY(0f)
        val markerView = CustomMarker(graph.context,
            R.layout.marker,xAxisLabel,0,null)
        graph.description.text = ""
        graph.marker = markerView
        graph.axisRight.xOffset = 10f
        graph.axisLeft.xOffset = 10f
        graph.legend.form = Legend.LegendForm.CIRCLE
        graph.xAxis.textColor =  Color.parseColor("#828282")
        graph.axisLeft.textColor = Color.parseColor("#828282")
        graph.axisRight.textColor = Color.parseColor("#828282")
        graph.legend.textColor = Color.parseColor("#828282")
        graph.xAxis.valueFormatter = (object : ValueFormatter() {
            val pattern = "dd MMM"
            private val mFormat = SimpleDateFormat(pattern)
            private val inputFormat = SimpleDateFormat("MM-dd")
            override fun getFormattedValue(value: Float): String {
                val millis = TimeUnit.HOURS.toMillis(value.toLong())
                return mFormat.format(inputFormat.parse(xAxisLabel[value.toInt()]))
            }
        })
        grafico = graph

    }

    fun graficorotodoEuropeo( grafic : PieChart,naz : DatiArray?,context : Context){
        grafic.setOnChartValueSelectedListener(this)
        grafic.setUsePercentValues(true)
        grafic.description.isEnabled = false
        grafic.setExtraOffsets(0F,0F,0F,0F)
        grafic.dragDecelerationFrictionCoef = 0.99f
        grafic.isDrawHoleEnabled = true
        grafic.setHoleColor(Color.parseColor("#1E2023"))
        grafic.transparentCircleRadius = 61f
        grafic.legend.horizontalAlignment = Legend.LegendHorizontalAlignment.CENTER
        var array = arrayListOf<Int>()
        naz?.confermati?.forEach {
            if(it.stato == ""){
                it.totale_casi?.last()?.let { it1 -> array.add(it1) }
            }
        }



        array.sortDescending()
        val name = naz?.let { Europei(array, it) }
        val c = arrayListOf<PieEntry>()
        var i = 0
        for  (i in 0..name!!.count()-1){
            c.add(PieEntry(array[i].toFloat(), name?.get(i)))
        }
        grafic.setEntryLabelColor(Color.TRANSPARENT)
        grafic.marker = CustomMarker(grafic.context,
            R.layout.marker,name,1,array)

        val dataset = PieDataSet(c,"")
        dataset.sliceSpace= 3f
        dataset.selectionShift = 5f
        dataset.setColors(Color.RED,Color.MAGENTA,Color.BLUE,Color.GREEN,Color.CYAN)
        grafic.legend.textColor = Color.parseColor("#828282")
        grafic.legend.form = Legend.LegendForm.CIRCLE
        val data = PieData(dataset)
        data.setValueTextSize(10f)
        grafic.legend.form = Legend.LegendForm.CIRCLE
        data.setValueTextColor(Color.WHITE)
        grafic.data = data
        //grafico1 = grafic

    }


    fun graficorotodoEuropeoStato(grafic : PieChart,naz : DatiArray?,context : Context,stato : String){
        grafic.setOnChartValueSelectedListener(this)
        grafic.setUsePercentValues(true)
        grafic.description.isEnabled = false
        grafic.setExtraOffsets(0F,0F,0F,0F)
        grafic.dragDecelerationFrictionCoef = 0.99f
        grafic.isDrawHoleEnabled = true
        grafic.setHoleColor(Color.parseColor("#1E2023"))
        grafic.transparentCircleRadius = 61f
        var array = arrayListOf<Int>()
        grafic.legend.horizontalAlignment = Legend.LegendHorizontalAlignment.CENTER
        var somma = 0
        naz?.confermati?.forEach {
            if(it.Region == stato && it.stato == ""){
                it.totale_casi?.last()?.let { it1 -> array.add(it1) }
            }
            if(it.Region != stato && it.stato == ""){
               somma+= it.totale_casi?.last()!!
            }
        }
        array.add(somma)





        val name = arrayListOf<String>(stato,"Europe")
        val c = arrayListOf<PieEntry>()
        var i = 0
        for  (i in 0..name!!.count()-1){
            c.add(PieEntry(array[i].toFloat(), name?.get(i)))
        }
        grafic.setEntryLabelColor(Color.TRANSPARENT)
        grafic.marker = CustomMarker(grafic.context,
            R.layout.marker,name,1,array)

        val dataset = PieDataSet(c,"")
        dataset.sliceSpace= 3f
        dataset.selectionShift = 5f
        dataset.setColors(Color.RED,Color.parseColor("#a0a0a0"))

        grafic.legend.textColor = Color.parseColor("#828282")
        grafic.legend.form = Legend.LegendForm.CIRCLE
        val data = PieData(dataset)
        data.setValueTextSize(10f)
        grafic.legend.form = Legend.LegendForm.CIRCLE
        data.setValueTextColor(Color.WHITE)
        grafic.data = data
        grafico1 = grafic

    }








    fun getLineeChartEuropeo(nazione : DatiArray, graph : LineChart,context : Context){
        val lista = arrayListOf<Entry>()
        val lista1 = arrayListOf<Entry>()
        val lista2 = arrayListOf<Entry>()
        var cont = 0
        var giorno : String = ""
        var quantita = 0
        var guariti = 0
        var deceduti = 0
        graph.xAxis.isGranularityEnabled = true
        graph.xAxis.setLabelCount(5,true)
        graph.legend.horizontalAlignment = Legend.LegendHorizontalAlignment.CENTER
        graph.xAxis.setAvoidFirstLastClipping(true)
        graph.animateX(1000)
        var xAxisLabel = ArrayList<String>(graph.size)
        for (i in 0..nazione.giorni.count()-1) {
            giorno =""
            quantita = 0
            guariti = 0
            deceduti = 0
            nazione?.confermati.forEach {

                quantita += it.totale_casi!![i]

            }
            nazione.deceduti.forEach {
                deceduti += it.totale_casi!![i]
            }

            nazione.guariti.forEach {
                guariti += it.totale_casi!![i]
            }

             giorno = nazione.giorni[i]


            xAxisLabel.add(giorno)
            if (quantita != null) {
                lista.add(Entry(cont.toFloat(), quantita.toFloat()))
            }
            if (guariti != null) {
                lista1.add(Entry(cont.toFloat(), guariti.toFloat()))
            }
            if (deceduti != null) {
                lista2.add(Entry(cont.toFloat(), deceduti.toFloat()))
            }
            cont += 1
        }


        graph.setOnChartValueSelectedListener(this)

        val lineDataset = LineDataSet(lista, context.getString(R.string.conf) )
        val lineDataset1 = LineDataSet(lista1,context.getString(R.string.guar) )
        val lineDataset2 = LineDataSet(lista2,context.getString(R.string.dec))

        graph.xAxis.position =XAxis.XAxisPosition.BOTTOM

        val data = LineData(lineDataset,lineDataset1,lineDataset2)
        setLine(lineDataset, "#5E6D92")
        setLine(lineDataset1, "#F44336")
        setLine(lineDataset, "#6BB564")
        graph.data = data
        graph.setScaleEnabled(false)
        // graph.viewPortHandler.setMaximumScaleX(2f)

        graph.viewPortHandler.setMaximumScaleY(0f)
        val markerView = CustomMarker(graph.context,
            R.layout.marker,xAxisLabel,0,null)
        graph.description.text = ""
        graph.marker = markerView
        graph.axisRight.xOffset = 10f
        graph.axisLeft.xOffset = 10f
        graph.legend.form = Legend.LegendForm.CIRCLE
        graph.xAxis.textColor =  Color.parseColor("#828282")
        graph.axisLeft.textColor = Color.parseColor("#828282")
        graph.axisRight.textColor = Color.parseColor("#828282")
        graph.legend.textColor = Color.parseColor("#828282")
       graph.xAxis.valueFormatter = (object : ValueFormatter() {
            val pattern = "dd MMM"
            private val mFormat = SimpleDateFormat(pattern)
            private val inputFormat = SimpleDateFormat("MM/dd/yy")
            override fun getFormattedValue(value: Float): String {
                val millis = TimeUnit.HOURS.toMillis(value.toLong())
                return mFormat.format(inputFormat.parse(xAxisLabel[value.toInt()]))
            }
        })
        grafico = graph

    }






    fun setLine(line : LineDataSet, color : String){
        println(color)
        line.setColor(Color.parseColor(color))
        line.valueTextSize = 9f
        line.setCircleColor(Color.parseColor(color))
    }
    fun graficorotodo( grafic : PieChart,naz : Collection<DatiRegionali>?){
         grafic.setUsePercentValues(true)
        grafic.description.isEnabled = false
        grafic.setExtraOffsets(0F,0F,0F,0F)
        grafic.dragDecelerationFrictionCoef = 0.99f
        grafic.isDrawHoleEnabled = true
        grafic.setHoleColor(Color.parseColor("#1E2023"))
        grafic.legend.horizontalAlignment = Legend.LegendHorizontalAlignment.CENTER
        grafic.transparentCircleRadius = 61f
        var array = arrayListOf<Int>()
        naz?.forEach {
            array.add(it.totale_casi)
        }

      array.sortDescending()
        val name = getNames(array,naz)
        val c = arrayListOf<PieEntry>()
        var i = 0
        for  (i in 0..name.count()-1){
            c.add(PieEntry(array[i].toFloat(),name[i]))
        }
        grafic.setEntryLabelColor(Color.TRANSPARENT)
        grafic.marker = CustomMarker(grafic.context,
            R.layout.marker,name,1,array)

        val dataset = PieDataSet(c,"")
        dataset.sliceSpace= 3f
        dataset.selectionShift = 5f
        dataset.setColors(Color.RED,Color.MAGENTA,Color.BLUE,Color.GREEN,Color.CYAN)
        grafic.legend.textColor = Color.parseColor("#828282")
        grafic.legend.form = Legend.LegendForm.CIRCLE
        val data = PieData(dataset)
        data.setValueTextSize(10f)
        grafic.legend.form = Legend.LegendForm.CIRCLE
        data.setValueTextColor(Color.WHITE)
        grafic.data = data
        grafic.setOnChartValueSelectedListener(this)
        grafico1 = grafic

    }


    fun getNames(array : ArrayList<Int>,naz : Collection<DatiRegionali>?) : ArrayList<String>{
        var i =0
        var name = arrayListOf<String>()
        for (i in 0..3){
        naz?.forEach {
            if (it.totale_casi == array[i]){
                name.add(it.denominazione_regione)
                println(it.denominazione_regione)
                println(it.totale_casi)
                println(array[i])

            }

        }
        }
        return name
    }

    fun Europei(array : ArrayList<Int>,naz : DatiArray) : ArrayList<String>{
        var i =0
        var name = arrayListOf<String>()
        for (i in 0..3){
            naz?.confermati.forEach {
                if (it.totale_casi!!.last() == array[i]){
                    if(!naz.giorniItaliani.isNullOrEmpty()){
                        for (j in 0..naz.giorniItaliani!!!!.count()-1){
                          if(it.Region == naz.giorniinglesi?.get(j)){
                              name.add(naz.giorniItaliani!![j])
                          }
                        }
                    }else {
                        it.Region?.let { it1 -> name.add(it1) }
                    }
                }
            }
        }
        return name
    }

    fun Europei1(array : ArrayList<Int>,naz : DatiArray) : ArrayList<String>{
        var i =0
        var name = arrayListOf<String>()

            naz?.confermati.forEach {
                if (it.totale_casi!!.last() == array[0]){
                    it.Region?.let { it1 -> name.add(it1) }
                }
            }

        return name
    }
    fun getNames1(array : ArrayList<Int>,naz : Collection<DatiProvincia>?) : ArrayList<String>{
        var i =0
        var name = arrayListOf<String>()
        for (i in 0..array.count().minus(1)){
            naz?.forEach {
                if (it.totale_casi == array[i] && it.data.contains(naz.last().data)){
                    name.add(it.denominazione_provincia)
                }
            }
        }
        return name
    }
    @RequiresApi(Build.VERSION_CODES.N)
    fun get_yesterdayDate(data : String,intero : Int) : ArrayList<String>{
        val i = 0
        val ar = arrayListOf<String>()
        for(i in 0..intero) {
            Thread.yield()
            val dateFormat: android.icu.text.SimpleDateFormat =
                android.icu.text.SimpleDateFormat("yyyy-MM-dd")
            val cal = Calendar.getInstance()
            val date: Date = dateFormat.parse(data)
            cal.time = date
            cal.add(Calendar.DATE, -i)
            ar.add(dateFormat.format(cal.time).toString())
        }
        return ar
    }
    //------------------------------  REGIONI
    @RequiresApi(Build.VERSION_CODES.N)
    fun getLineeChartRegione(nazione : Collection<DatiRegionali>?, graph : LineChart, nome : String,contesto : Context){
        val lista = arrayListOf<Entry>()
        val lista1 = arrayListOf<Entry>()
        val lista2 = arrayListOf<Entry>()
        var cont = 0
        graph.animateX(1000)
        var xAxisLabel = ArrayList<String>(graph.size)
        graph.xAxis.setLabelCount(5,true)
        graph.legend.horizontalAlignment = Legend.LegendHorizontalAlignment.CENTER

        graph.xAxis.setAvoidFirstLastClipping(true)
        graph.legend.form = Legend.LegendForm.CIRCLE
        nazione?.forEach {

                if (it.denominazione_regione == nome)  {
                    val giorno = it.data.subSequence(5, 10).toString()
                    val quantita = it.totale_casi.toFloat()
                    val deceduti = it.deceduti.toFloat()
                    val guariti = it.dimessi_guariti.toFloat()

                    xAxisLabel.add(giorno)
                    lista.add(Entry(cont.toFloat(), quantita))
                    lista1.add(Entry(cont.toFloat(), guariti))
                    lista2.add(Entry(cont.toFloat(), deceduti))
                    cont += 1



                }
            }



        graph.setOnChartValueSelectedListener(this)

        val lineDataset = LineDataSet(lista,contesto.getString(R.string.conf))
        val lineDataset1 = LineDataSet(lista1,contesto.getString(R.string.guar))
        val lineDataset2 = LineDataSet(lista2,contesto.getString(R.string.dec))



        graph.xAxis.position =XAxis.XAxisPosition.BOTTOM

        val data = LineData(lineDataset,lineDataset1,lineDataset2)
        setLine(lineDataset, "#5E6D92")
        setLine(lineDataset1, "#F44336")
        setLine(lineDataset, "#6BB564")
        graph.data = data
        graph.setScaleEnabled(false)
        // graph.viewPortHandler.setMaximumScaleX(2f)

        graph.viewPortHandler.setMaximumScaleY(0f)
        val markerView = CustomMarker(graph.context,
            R.layout.marker,xAxisLabel,0,null)
        graph.description.text = ""
        graph.marker = markerView
        graph.axisRight.xOffset = 10f
        graph.axisLeft.xOffset = 10f
        graph.legend.form = Legend.LegendForm.CIRCLE
        graph.xAxis.textColor =  Color.parseColor("#828282")
        graph.axisLeft.textColor = Color.parseColor("#828282")
        graph.axisRight.textColor = Color.parseColor("#828282")
        graph.legend.textColor = Color.parseColor("#828282")

        graph.xAxis.valueFormatter = (object : ValueFormatter() {
            val pattern = "dd MMM"
            private val mFormat = SimpleDateFormat(pattern)
            private val inputFormat = SimpleDateFormat("MM-dd")
            override fun getFormattedValue(value: Float): String {
                val millis = TimeUnit.HOURS.toMillis(value.toLong())
                return mFormat.format(inputFormat.parse(xAxisLabel[value.toInt()]))
            }
        })
        grafico = graph

    }


    fun getLineeChartEuropaStato(naz : DatiArray, graph : LineChart, nome : String,contesto : Context){
        val lista = arrayListOf<Entry>()
        val lista1 = arrayListOf<Entry>()
        val lista2 = arrayListOf<Entry>()
        var cont = 0
        graph.xAxis.isGranularityEnabled = true
        graph.xAxis.setGranularity(0f);
        graph.animateX(1000)
        var xAxisLabel = ArrayList<String>(graph.size)
        graph.xAxis.setLabelCount(5,true)
        graph.legend.horizontalAlignment = Legend.LegendHorizontalAlignment.CENTER


        graph.xAxis.setAvoidFirstLastClipping(true)


        graph.legend.form = Legend.LegendForm.CIRCLE
        for (i in 0..naz.guariti.count()-1) {

            if (naz.confermati[i].stato == "" && naz.confermati[i].Region == nome) {
                for (j in 0..naz.confermati[i].totale_casi!!.count()-1){
                    val giorno = naz.giorni[j]
                    val quantita = naz.confermati[i].totale_casi!![j].toFloat()
                    val deceduti = naz.deceduti[i].totale_casi!![j].toFloat()
                    val guariti = naz.guariti[i].totale_casi!![j].toFloat()

                    xAxisLabel.add(giorno)
                    lista.add(Entry(j.toFloat(), quantita))
                    lista1.add(Entry(j.toFloat(), guariti))
                    lista2.add(Entry(j.toFloat(), deceduti))
                }

                cont += 1


            }
        }



        graph.xAxis.position =XAxis.XAxisPosition.BOTTOM
        graph.setOnChartValueSelectedListener(this)

        val lineDataset = LineDataSet(lista,contesto.getString(R.string.conf))
        val lineDataset1 = LineDataSet(lista1,contesto.getString(R.string.guar))
        val lineDataset2 = LineDataSet(lista2,contesto.getString(R.string.dec))

        val data = LineData(lineDataset,lineDataset1,lineDataset2)
        setLine(lineDataset, "#5E6D92")
        setLine(lineDataset1, "#F44336")
        setLine(lineDataset, "#6BB564")
        graph.data = data
        graph.setScaleEnabled(false)
        // graph.viewPortHandler.setMaximumScaleX(2f)
        graph.xAxis.textColor =  Color.parseColor("#828282")
        graph.axisLeft.textColor = Color.parseColor("#828282")
        graph.axisRight.textColor = Color.parseColor("#828282")
        graph.legend.textColor = Color.parseColor("#828282")
        graph.viewPortHandler.setMaximumScaleY(0f)
        val markerView = CustomMarker(graph.context,
            R.layout.marker,xAxisLabel,0,null)
        graph.description.text = ""
        graph.marker = markerView
        graph.axisRight.xOffset = 10f
        graph.axisLeft.xOffset = 10f
        graph.xAxis.yOffset = 12f

        graph.xAxis.valueFormatter = (object : ValueFormatter() {
            val pattern = "dd MMM"
            private val mFormat = SimpleDateFormat(pattern)
            private val inputFormat = SimpleDateFormat("MM/dd/yy")
            override fun getFormattedValue(value: Float): String {
                val millis = TimeUnit.HOURS.toMillis(value.toLong())
                return mFormat.format(inputFormat.parse(xAxisLabel[value.toInt()]))
            }
        })
        grafico = graph

    }



    fun graficoRotondoRegione(grafic : PieChart, naz : Collection<DatiProvincia>?, nome : String){
        grafic.setOnChartValueSelectedListener(this)
        grafic.setUsePercentValues(true)
        grafic.description.isEnabled = false
        grafic.setExtraOffsets(0F,0F,0F,0F)
        grafic.dragDecelerationFrictionCoef = 0.99f
        grafic.isDrawHoleEnabled = true
        grafic.legend.horizontalAlignment = Legend.LegendHorizontalAlignment.CENTER
        grafic.setHoleColor(Color.parseColor("#1E2023"))
        grafic.transparentCircleRadius = 61f
        grafic.legend.form = Legend.LegendForm.CIRCLE
        var array = arrayListOf<Int>()
        naz?.forEach {
            if(it.denominazione_regione == nome && it.data.contains(naz.last().data) )
            array.add(it.totale_casi)
        }

        array.sortDescending()
        val name = getNames1(array,naz)
        val c = arrayListOf<PieEntry>()
        var i = 0
        for  (i in 0..array.count()-1){
            c.add(PieEntry(array[i].toFloat(),name[i]))
        }
        grafic.setEntryLabelColor(Color.TRANSPARENT)
        grafic.marker = CustomMarker(grafic.context,
            R.layout.marker,name,1,array)
        grafic.highlightValue(null)
        val dataset = PieDataSet(c,"")
        dataset.sliceSpace= 3f
        dataset.selectionShift = 5f
        dataset.setColors(Color.RED,Color.MAGENTA,Color.BLUE,Color.GREEN,Color.CYAN)
        grafic.legend.textColor = Color.parseColor("#828282")
        val data = PieData(dataset)
        data.setValueTextSize(10f)
        data.setValueTextColor(Color.WHITE)
        grafic.data = data
        grafico1 = grafic

    }
    fun getBarChartRegione(regione : Collection<DatiRegionali>?, graph: BarChart, scelta: Int, nomeReg : String,contesto:Context,btn1:Button,btn2:Button,btn3:Button){

        graph.legend.form = Legend.LegendForm.EMPTY
        graph.axisLeft.isEnabled = false
        graph.xAxis.position =XAxis.XAxisPosition.BOTTOM
        graph.legend.horizontalAlignment = Legend.LegendHorizontalAlignment.LEFT
        graph.legend.verticalAlignment = Legend.LegendVerticalAlignment.TOP
        graph.legend.setDrawInside(false)
        graph.legend.yOffset = 10f
        when(scelta){
            0->{
                btn1.setOnClickListener {
                    resetChart(graph)
                    var array = arrayListOf<Int>()
                    var nomi = arrayListOf<String>()
                    var i = 7
                    var cont = 0
                    val index = arrayListOf<Int>()
                    val arr = regione?.last()?.data?.substring(0,10)?.let { get_yesterdayDate(it,7) }
                    if (arr != null) {
                        i = arr.count()-1
                    }
                    Thread.yield()
                    for (j in i downTo 0) {
                        Thread.yield()
                        if (regione != null) {
                             regione.forEach {
                                Thread.yield()

                                if (arr?.get(j)?.let { it1 -> it.data.contains(it1) }!! && it.denominazione_regione == nomeReg) {
                                    array.add(it.totale_casi)
                                    index.add(cont)

                                    return@forEach
                                }
                                cont += 1
                            }
                        }
                        cont = 0

                    }



                    for (i in 1..index.count()-1){
                        regione?.elementAt(index[i])?.data?.substring(5,10)?.let { nomi.add(it) }
                    }
                    val entries: ArrayList<BarEntry> = ArrayList()
                    array = getarray(array)
                    for (i in 0..6){
                        entries.add(BarEntry(i.toFloat(),array[i].toFloat()))
                    }
                    val bardataset = BarDataSet(entries, contesto.getString(R.string.confermatisetti))
                    bardataset.valueTextSize = 10f
                    bardataset.valueTextColor = Color.parseColor("#828282")
                    val data = BarData(bardataset)
                    graph.xAxis.setDrawGridLines(false)
                    graph.axisRight.setDrawGridLines(false)
                    graph.axisLeft.setDrawGridLines(false)
                    graph.setData(data); // set the data and list of lables into chart
                    graph.setScaleEnabled(false)
                    graph.description.text = ""
                    graph.axisRight.xOffset = 10f
                    graph.axisLeft.xOffset = 10f
                    bardataset.setDrawValues(true)
                    graph.axisLeft.textColor = Color.parseColor("#828282")
                    graph.axisRight.textColor = Color.parseColor("#828282")
                    bardataset.color = Color.parseColor("#E91E62")
                    //graph.xAxis.valueFormatter = IndexAxisValueFormatter(nomi)
                    // graph.xAxis.granularity = 1f
                    // graph.xAxis.isGranularityEnabled = true
                    graph.legend.textColor =  Color.parseColor("#828282")
                    graph.animateY(1000)
                    graph.xAxis.textColor =  Color.parseColor("#828282")
                    graph.xAxis.valueFormatter = (object : ValueFormatter() {
                        val pattern = "dd MMM"
                        private val mFormat = SimpleDateFormat(pattern)
                        private val inputFormat = SimpleDateFormat("MM-dd")
                        override fun getFormattedValue(value: Float): String {
                            val millis = TimeUnit.HOURS.toMillis(value.toLong())
                            return mFormat.format(inputFormat.parse(nomi[value.toInt()]))
                        }
                    })
                }
                btn1.performClick()
                btn2.setOnClickListener {
                    resetChart(graph)
                    var array = arrayListOf<Int>()
                    var nomi = arrayListOf<String>()
                    var i = 7
                    var cont = 0
                    val index = arrayListOf<Int>()
                    val arr = regione?.last()?.data?.substring(0,10)?.let { get_yesterdayDate(it,31) }
                    if (arr != null) {
                        i = arr.count()-1
                    }

                    while(i>=0) {
                        if (regione != null) {
                            regione.forEach {

                                if (arr?.get(i)?.let { it1 -> it.data.contains(it1) }!! && it.denominazione_regione == nomeReg) {
                                    array.add(it.totale_casi)
                                    index.add(cont)
                                }
                                cont += 1
                            }
                        }
                        cont = 0
                        i-=1
                    }



                    for (i in 1..index.count()-1){
                        regione?.elementAt(index[i])?.data?.substring(5,10)?.let { nomi.add(it) }
                    }
                    val entries: ArrayList<BarEntry> = ArrayList()
                    array = getarray(array)
                    for (i in 0..30){
                        entries.add(BarEntry(i.toFloat(),array[i].toFloat()))
                    }

                    val bardataset = BarDataSet(entries, contesto.getString(R.string.confermatimese))
                    bardataset.setDrawValues(false)
                    bardataset.valueTextSize = 10f
                    bardataset.valueTextColor = Color.parseColor("#828282")
                    val data = BarData(bardataset)
                    graph.xAxis.setDrawGridLines(false)
                    graph.axisRight.setDrawGridLines(false)
                    graph.axisLeft.setDrawGridLines(false)
                    graph.setData(data); // set the data and list of lables into chart
                    graph.setScaleEnabled(false)
                    graph.description.text = ""
                    graph.axisRight.xOffset = 10f
                    graph.axisLeft.xOffset = 10f
                    graph.axisLeft.textColor = Color.parseColor("#828282")
                    graph.axisRight.textColor = Color.parseColor("#828282")
                    bardataset.color = Color.parseColor("#E91E62")
                    //graph.xAxis.valueFormatter = IndexAxisValueFormatter(nomi)
                    // graph.xAxis.granularity = 1f
                    // graph.xAxis.isGranularityEnabled = true
                    graph.legend.textColor =  Color.parseColor("#828282")
                    graph.animateY(1000)
                    graph.xAxis.textColor =  Color.parseColor("#828282")
                    graph.xAxis.valueFormatter = (object : ValueFormatter() {
                        val pattern = "dd MMM"
                        private val mFormat = SimpleDateFormat(pattern)
                        private val inputFormat = SimpleDateFormat("MM-dd")
                        override fun getFormattedValue(value: Float): String {
                            val millis = TimeUnit.HOURS.toMillis(value.toLong())
                            return mFormat.format(inputFormat.parse(nomi[value.toInt()]))
                        }
                    })
                }
                btn3.setOnClickListener {
                    resetChart(graph)
                    var array = arrayListOf<Int>()
                    var nomi = arrayListOf<String>()
                    var i = 7
                    var cont = 0
                    val index = arrayListOf<Int>()

                    val arr = regione?.last()?.data?.substring(0,10)?.let { get_yesterdayDate(it,(regione.count()/21)-1) }
                    if (arr != null) {
                        i = arr.count()-1
                    }

                    while(i>=0) {
                        if (regione != null) {
                            regione.forEach {

                                if (arr?.get(i)?.let { it1 -> it.data.contains(it1) }!! && it.denominazione_regione == nomeReg) {
                                    array.add(it.totale_casi)
                                    index.add(cont)
                                }
                                cont += 1
                            }
                        }
                        cont = 0
                        i-=1
                    }



                    for (i in 1..index.count()-1){
                        regione?.elementAt(index[i])?.data?.substring(5,10)?.let { nomi.add(it) }
                    }
                    val entries: ArrayList<BarEntry> = ArrayList()
                    array = getarray(array)
                    for (i in 0..index.count()-2){
                        entries.add(BarEntry(i.toFloat(),array[i].toFloat()))
                    }

                    val bardataset = BarDataSet(entries, contesto.getString(R.string.confermatianno))
                    bardataset.setDrawValues(false)
                    bardataset.valueTextSize = 10f
                    bardataset.valueTextColor = Color.parseColor("#828282")
                    val data = BarData(bardataset)
                    graph.xAxis.setDrawGridLines(false)
                    graph.axisRight.setDrawGridLines(false)
                    graph.axisLeft.setDrawGridLines(false)
                    graph.setData(data); // set the data and list of lables into chart
                    graph.setScaleEnabled(false)
                    graph.description.text = ""
                    graph.axisRight.xOffset = 10f
                    graph.axisLeft.xOffset = 10f
                    graph.axisLeft.textColor = Color.parseColor("#828282")
                    graph.axisRight.textColor = Color.parseColor("#828282")
                    bardataset.color = Color.parseColor("#E91E62")
                    //graph.xAxis.valueFormatter = IndexAxisValueFormatter(nomi)
                    // graph.xAxis.granularity = 1f
                    // graph.xAxis.isGranularityEnabled = true
                    graph.legend.textColor =  Color.parseColor("#828282")
                    graph.animateY(1000)
                    graph.xAxis.textColor =  Color.parseColor("#828282")
                    graph.xAxis.valueFormatter = (object : ValueFormatter() {
                        val pattern = "dd MMM"
                        private val mFormat = SimpleDateFormat(pattern)
                        private val inputFormat = SimpleDateFormat("MM-dd")
                        override fun getFormattedValue(value: Float): String {
                            val millis = TimeUnit.HOURS.toMillis(value.toLong())
                            return mFormat.format(inputFormat.parse(nomi[value.toInt()]))
                        }
                    })
                }

            }
            1 ->{
                btn1.setOnClickListener {
                    resetChart(graph)
                    var array = arrayListOf<Int>()
                    var nomi = arrayListOf<String>()
                    var i = 7
                    var cont = 0
                    val index = arrayListOf<Int>()
                    val arr = regione?.last()?.data?.substring(0,10)?.let { get_yesterdayDate(it,7) }
                    if (arr != null) {
                        i = arr.count()-1
                    }
                    Thread.yield()
                    for (j in i downTo 0) {
                        Thread.yield()
                        if (regione != null) {
                            regione.forEach {
                                Thread.yield()

                                if (arr?.get(j)?.let { it1 -> it.data.contains(it1) }!! && it.denominazione_regione == nomeReg) {
                                    array.add(it.dimessi_guariti)
                                    index.add(cont)
                                }
                                cont += 1
                            }
                        }
                        cont = 0

                    }



                    for (i in 1..index.count()-1){
                        regione?.elementAt(index[i])?.data?.substring(5,10)?.let { nomi.add(it) }
                    }
                    val entries: ArrayList<BarEntry> = ArrayList()
                    array = getarray(array)
                    for (i in 0..6){
                        entries.add(BarEntry(i.toFloat(),array[i].toFloat()))
                    }
                    val bardataset = BarDataSet(entries, contesto.getString(R.string.guaritisetti))
                    bardataset.valueTextSize = 10f
                    bardataset.valueTextColor = Color.parseColor("#828282")
                    val data = BarData(bardataset)
                    graph.xAxis.setDrawGridLines(false)
                    graph.axisRight.setDrawGridLines(false)
                    graph.axisLeft.setDrawGridLines(false)
                    graph.setData(data); // set the data and list of lables into chart
                    graph.setScaleEnabled(false)
                    graph.description.text = ""
                    graph.axisRight.xOffset = 10f
                    graph.axisLeft.xOffset = 10f
                    bardataset.setDrawValues(true)
                    graph.axisLeft.textColor = Color.parseColor("#828282")
                    graph.axisRight.textColor = Color.parseColor("#828282")
                    bardataset.color = Color.parseColor("#E91E62")
                    //graph.xAxis.valueFormatter = IndexAxisValueFormatter(nomi)
                    // graph.xAxis.granularity = 1f
                    // graph.xAxis.isGranularityEnabled = true
                    graph.legend.textColor =  Color.parseColor("#828282")
                    graph.animateY(1000)
                    graph.xAxis.textColor =  Color.parseColor("#828282")
                    graph.xAxis.valueFormatter = (object : ValueFormatter() {
                        val pattern = "dd MMM"
                        private val mFormat = SimpleDateFormat(pattern)
                        private val inputFormat = SimpleDateFormat("MM-dd")
                        override fun getFormattedValue(value: Float): String {
                            val millis = TimeUnit.HOURS.toMillis(value.toLong())
                            return mFormat.format(inputFormat.parse(nomi[value.toInt()]))
                        }
                    })
                }
                btn1.performClick()
                btn2.setOnClickListener {
                    resetChart(graph)
                    var array = arrayListOf<Int>()
                    var nomi = arrayListOf<String>()
                    var i = 7
                    var cont = 0
                    val index = arrayListOf<Int>()
                    val arr = regione?.last()?.data?.substring(0,10)?.let { get_yesterdayDate(it,31) }
                    if (arr != null) {
                        i = arr.count()-1
                    }

                    while(i>=0) {
                        if (regione != null) {
                            regione.forEach {

                                if (arr?.get(i)?.let { it1 -> it.data.contains(it1) }!! && it.denominazione_regione == nomeReg) {
                                    array.add(it.dimessi_guariti)
                                    index.add(cont)
                                }
                                cont += 1
                            }
                        }
                        cont = 0
                        i-=1
                    }



                    for (i in 1..index.count()-1){
                        regione?.elementAt(index[i])?.data?.substring(5,10)?.let { nomi.add(it) }
                    }
                    val entries: ArrayList<BarEntry> = ArrayList()
                    array = getarray(array)
                    for (i in 0..30){
                        entries.add(BarEntry(i.toFloat(),array[i].toFloat()))
                    }

                    val bardataset = BarDataSet(entries, contesto.getString(R.string.guaritimese))
                    bardataset.setDrawValues(false)
                    bardataset.valueTextSize = 10f
                    bardataset.valueTextColor = Color.parseColor("#828282")
                    val data = BarData(bardataset)
                    graph.xAxis.setDrawGridLines(false)
                    graph.axisRight.setDrawGridLines(false)
                    graph.axisLeft.setDrawGridLines(false)
                    graph.setData(data); // set the data and list of lables into chart
                    graph.setScaleEnabled(false)
                    graph.description.text = ""
                    graph.axisRight.xOffset = 10f
                    graph.axisLeft.xOffset = 10f
                    graph.axisLeft.textColor = Color.parseColor("#828282")
                    graph.axisRight.textColor = Color.parseColor("#828282")
                    bardataset.color = Color.parseColor("#E91E62")
                    //graph.xAxis.valueFormatter = IndexAxisValueFormatter(nomi)
                    // graph.xAxis.granularity = 1f
                    // graph.xAxis.isGranularityEnabled = true
                    graph.legend.textColor =  Color.parseColor("#828282")
                    graph.animateY(1000)
                    graph.xAxis.textColor =  Color.parseColor("#828282")
                    graph.xAxis.valueFormatter = (object : ValueFormatter() {
                        val pattern = "dd MMM"
                        private val mFormat = SimpleDateFormat(pattern)
                        private val inputFormat = SimpleDateFormat("MM-dd")
                        override fun getFormattedValue(value: Float): String {
                            val millis = TimeUnit.HOURS.toMillis(value.toLong())
                            return mFormat.format(inputFormat.parse(nomi[value.toInt()]))
                        }
                    })
                }
                btn3.setOnClickListener {
                    resetChart(graph)
                    var array = arrayListOf<Int>()
                    var nomi = arrayListOf<String>()
                    var i = 7
                    var cont = 0
                    val index = arrayListOf<Int>()

                    val arr = regione?.last()?.data?.substring(0,10)?.let { get_yesterdayDate(it,(regione.count()/21)-1) }
                    if (arr != null) {
                        i = arr.count()-1
                    }

                    while(i>=0) {
                        if (regione != null) {
                            regione.forEach {

                                if (arr?.get(i)?.let { it1 -> it.data.contains(it1) }!! && it.denominazione_regione == nomeReg) {
                                    array.add(it.dimessi_guariti)
                                    index.add(cont)
                                }
                                cont += 1
                            }
                        }
                        cont = 0
                        i-=1
                    }



                    for (i in 1..index.count()-1){
                        regione?.elementAt(index[i])?.data?.substring(5,10)?.let { nomi.add(it) }
                    }
                    val entries: ArrayList<BarEntry> = ArrayList()
                    array = getarray(array)
                    for (i in 0..index.count()-2){
                        entries.add(BarEntry(i.toFloat(),array[i].toFloat()))
                    }

                    val bardataset = BarDataSet(entries, contesto.getString(R.string.guaritianno))
                    bardataset.setDrawValues(false)
                    bardataset.valueTextSize = 10f
                    bardataset.valueTextColor = Color.parseColor("#828282")
                    val data = BarData(bardataset)
                    graph.xAxis.setDrawGridLines(false)
                    graph.axisRight.setDrawGridLines(false)
                    graph.axisLeft.setDrawGridLines(false)
                    graph.setData(data); // set the data and list of lables into chart
                    graph.setScaleEnabled(false)
                    graph.description.text = ""
                    graph.axisRight.xOffset = 10f
                    graph.axisLeft.xOffset = 10f
                    graph.axisLeft.textColor = Color.parseColor("#828282")
                    graph.axisRight.textColor = Color.parseColor("#828282")
                    bardataset.color = Color.parseColor("#E91E62")
                    //graph.xAxis.valueFormatter = IndexAxisValueFormatter(nomi)
                    // graph.xAxis.granularity = 1f
                    // graph.xAxis.isGranularityEnabled = true
                    graph.legend.textColor =  Color.parseColor("#828282")
                    graph.animateY(1000)
                    graph.xAxis.textColor =  Color.parseColor("#828282")
                    graph.xAxis.valueFormatter = (object : ValueFormatter() {
                        val pattern = "dd MMM"
                        private val mFormat = SimpleDateFormat(pattern)
                        private val inputFormat = SimpleDateFormat("MM-dd")
                        override fun getFormattedValue(value: Float): String {
                            val millis = TimeUnit.HOURS.toMillis(value.toLong())
                            return mFormat.format(inputFormat.parse(nomi[value.toInt()]))
                        }
                    })
                }
            }
            2->{
                btn1.setOnClickListener {
                    resetChart(graph)
                    var array = arrayListOf<Int>()
                    var nomi = arrayListOf<String>()
                    var i = 7
                    var cont = 0
                    val index = arrayListOf<Int>()
                    val arr = regione?.last()?.data?.substring(0,10)?.let { get_yesterdayDate(it,7) }
                    if (arr != null) {
                        i = arr.count()-1
                    }
                    Thread.yield()
                    for (j in i downTo 0) {
                        Thread.yield()
                        if (regione != null) {
                            regione.forEach {
                                Thread.yield()

                                if (arr?.get(j)?.let { it1 -> it.data.contains(it1) }!! && it.denominazione_regione == nomeReg) {
                                    array.add(it.deceduti)
                                    index.add(cont)
                                }
                                cont += 1
                            }
                        }
                        cont = 0

                    }



                    for (i in 1..index.count()-1){
                        regione?.elementAt(index[i])?.data?.substring(5,10)?.let { nomi.add(it) }
                    }
                    val entries: ArrayList<BarEntry> = ArrayList()
                    array = getarray(array)
                    for (i in 0..6){
                        entries.add(BarEntry(i.toFloat(),array[i].toFloat()))
                    }
                    val bardataset = BarDataSet(entries, contesto.getString(R.string.decedutisett))
                    bardataset.valueTextSize = 10f
                    bardataset.valueTextColor = Color.parseColor("#828282")
                    val data = BarData(bardataset)
                    graph.xAxis.setDrawGridLines(false)
                    graph.axisRight.setDrawGridLines(false)
                    graph.axisLeft.setDrawGridLines(false)
                    graph.setData(data); // set the data and list of lables into chart
                    graph.setScaleEnabled(false)
                    graph.description.text = ""
                    graph.axisRight.xOffset = 10f
                    graph.axisLeft.xOffset = 10f
                    bardataset.setDrawValues(true)
                    graph.axisLeft.textColor = Color.parseColor("#828282")
                    graph.axisRight.textColor = Color.parseColor("#828282")
                    bardataset.color = Color.parseColor("#E91E62")
                    //graph.xAxis.valueFormatter = IndexAxisValueFormatter(nomi)
                    // graph.xAxis.granularity = 1f
                    // graph.xAxis.isGranularityEnabled = true
                    graph.legend.textColor =  Color.parseColor("#828282")
                    graph.animateY(1000)
                    graph.xAxis.textColor =  Color.parseColor("#828282")
                    graph.xAxis.valueFormatter = (object : ValueFormatter() {
                        val pattern = "dd MMM"
                        private val mFormat = SimpleDateFormat(pattern)
                        private val inputFormat = SimpleDateFormat("MM-dd")
                        override fun getFormattedValue(value: Float): String {
                            val millis = TimeUnit.HOURS.toMillis(value.toLong())
                            return mFormat.format(inputFormat.parse(nomi[value.toInt()]))
                        }
                    })
                }
                btn1.performClick()
                btn2.setOnClickListener {
                    resetChart(graph)
                    var array = arrayListOf<Int>()
                    var nomi = arrayListOf<String>()
                    var i = 7
                    var cont = 0
                    val index = arrayListOf<Int>()
                    val arr = regione?.last()?.data?.substring(0,10)?.let { get_yesterdayDate(it,31) }
                    if (arr != null) {
                        i = arr.count()-1
                    }

                    while(i>=0) {
                        if (regione != null) {
                            regione.forEach {

                                if (arr?.get(i)?.let { it1 -> it.data.contains(it1) }!! && it.denominazione_regione == nomeReg) {
                                    array.add(it.deceduti)
                                    index.add(cont)
                                }
                                cont += 1
                            }
                        }
                        cont = 0
                        i-=1
                    }



                    for (i in 1..index.count()-1){
                        regione?.elementAt(index[i])?.data?.substring(5,10)?.let { nomi.add(it) }
                    }
                    val entries: ArrayList<BarEntry> = ArrayList()
                    array = getarray(array)
                    for (i in 0..30){
                        entries.add(BarEntry(i.toFloat(),array[i].toFloat()))
                    }

                    val bardataset = BarDataSet(entries, contesto.getString(R.string.decedutimese))
                    bardataset.setDrawValues(false)
                    bardataset.valueTextSize = 10f
                    bardataset.valueTextColor = Color.parseColor("#828282")
                    val data = BarData(bardataset)
                    graph.xAxis.setDrawGridLines(false)
                    graph.axisRight.setDrawGridLines(false)
                    graph.axisLeft.setDrawGridLines(false)
                    graph.setData(data); // set the data and list of lables into chart
                    graph.setScaleEnabled(false)
                    graph.description.text = ""
                    graph.axisRight.xOffset = 10f
                    graph.axisLeft.xOffset = 10f
                    graph.axisLeft.textColor = Color.parseColor("#828282")
                    graph.axisRight.textColor = Color.parseColor("#828282")
                    bardataset.color = Color.parseColor("#E91E62")
                    //graph.xAxis.valueFormatter = IndexAxisValueFormatter(nomi)
                    // graph.xAxis.granularity = 1f
                     //graph.xAxis.isGranularityEnabled = true
                    graph.legend.textColor =  Color.parseColor("#828282")
                    graph.animateY(1000)
                    graph.xAxis.textColor =  Color.parseColor("#828282")
                    graph.xAxis.valueFormatter = (object : ValueFormatter() {
                        val pattern = "dd MMM"
                        private val mFormat = SimpleDateFormat(pattern)
                        private val inputFormat = SimpleDateFormat("MM-dd")
                        override fun getFormattedValue(value: Float): String {
                            val millis = TimeUnit.HOURS.toMillis(value.toLong())
                            return mFormat.format(inputFormat.parse(nomi[value.toInt()]))
                        }
                    })
                }
                btn3.setOnClickListener {
                    resetChart(graph)
                    var array = arrayListOf<Int>()
                    var nomi = arrayListOf<String>()
                    var i = 7
                    var cont = 0
                    val index = arrayListOf<Int>()

                    val arr = regione?.last()?.data?.substring(0,10)?.let { get_yesterdayDate(it,(regione.count()/21)-1) }
                    if (arr != null) {
                        i = arr.count()-1
                    }

                    while(i>=0) {
                        if (regione != null) {
                            regione.forEach {

                                if (arr?.get(i)?.let { it1 -> it.data.contains(it1) }!! && it.denominazione_regione == nomeReg) {
                                    array.add(it.deceduti)
                                    index.add(cont)
                                }
                                cont += 1
                            }
                        }
                        cont = 0
                        i-=1
                    }



                    for (i in 1..index.count()-1){
                        regione?.elementAt(index[i])?.data?.substring(5,10)?.let { nomi.add(it) }
                    }
                    val entries: ArrayList<BarEntry> = ArrayList()
                    array = getarray(array)
                    for (i in 0..index.count()-2){
                        entries.add(BarEntry(i.toFloat(),array[i].toFloat()))
                    }

                    val bardataset = BarDataSet(entries, contesto.getString(R.string.decedutianno))
                    bardataset.setDrawValues(false)
                    bardataset.valueTextSize = 10f
                    bardataset.valueTextColor = Color.parseColor("#828282")
                    val data = BarData(bardataset)
                    graph.xAxis.setDrawGridLines(false)
                    graph.axisRight.setDrawGridLines(false)
                    graph.axisLeft.setDrawGridLines(false)
                    graph.setData(data); // set the data and list of lables into chart
                    graph.setScaleEnabled(false)
                    graph.description.text = ""
                    graph.axisRight.xOffset = 10f
                    graph.axisLeft.xOffset = 10f
                    graph.axisLeft.textColor = Color.parseColor("#828282")
                    graph.axisRight.textColor = Color.parseColor("#828282")
                    bardataset.color = Color.parseColor("#E91E62")
                    //graph.xAxis.valueFormatter = IndexAxisValueFormatter(nomi)
                    // graph.xAxis.granularity = 1f
                    // graph.xAxis.isGranularityEnabled = true
                    graph.legend.textColor =  Color.parseColor("#828282")
                    graph.animateY(1000)
                    graph.xAxis.textColor =  Color.parseColor("#828282")
                    graph.xAxis.valueFormatter = (object : ValueFormatter() {
                        val pattern = "dd MMM"
                        private val mFormat = SimpleDateFormat(pattern)
                        private val inputFormat = SimpleDateFormat("MM-dd")
                        override fun getFormattedValue(value: Float): String {
                            val millis = TimeUnit.HOURS.toMillis(value.toLong())
                            return mFormat.format(inputFormat.parse(nomi[value.toInt()]))
                        }
                    })
                }

            }
            else -> "fatti in culo"
        }
    }




    override fun onNothingSelected() {
       //To change body of created functions use File | Settings | File Templates.
    }

    override fun onValueSelected(e: Entry?, h: Highlight?) {
        println(h)
        if (this::timer.isInitialized) {
            timer.cancel()
        timer = Timer("SettingUp", false).schedule(2000) {
            grafico.highlightValue(null)


        }
        }else{
            timer1.cancel()
            timer1 = Timer("SettingUp1", false).schedule(2000) {
                grafico1.highlightValues(null)
                println("gatto")
        }
    }


    }

}


