import android.content.Context
import android.graphics.Canvas
import android.view.ViewManager
import com.github.mikephil.charting.components.MarkerView
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.highlight.Highlight
import com.github.mikephil.charting.utils.MPPointF
import kotlinx.android.synthetic.main.marker.view.*
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList
import kotlin.concurrent.schedule


class CustomMarker(context: Context, layoutResource: Int,nomi : ArrayList<String>,scelta : Int,dati : ArrayList<Int>?):  MarkerView(context, layoutResource) {
    var mXLabels: ArrayList<String> = nomi
    var scel = scelta
    val datii = dati
    override fun refreshContent(entry: Entry?, highlight: Highlight?) {
        val value = entry?.y?.toDouble() ?: 0.0
        var resText = ""
        val pattern = "dd MMM"
        if (entry?.x?.toInt()!! < mXLabels.count()) {
            when (scel) {
                0 -> {
                    var inputFormat = SimpleDateFormat()
                    val mFormat = SimpleDateFormat(pattern)
                    if(mXLabels[0].contains("/")){
                         inputFormat = SimpleDateFormat("MM/dd/yy")
                    }else{
                        inputFormat = SimpleDateFormat("MM-dd")
                    }

                    val dat =
                        mFormat.format(inputFormat.parse(entry?.x?.toInt()?.let { mXLabels?.get(it) }))
                    if (value.toString().length > 8) {

                        resText =  dat+" : " + value.toString().substring(0, 7)
                    } else {

                        if (entry != null) {
                            resText = dat+" : " + value.toString()
                        }
                    }
                    tvPrice.text = resText

                }
                1 -> {

                 val i = 0
                    var nome = ""
                    var g = datii?.count() as Int
                    g-=1
                    for (i in 0..g){
                        if (datii[i] == entry.y.toInt()){
                           nome = mXLabels[i]
                        }
                    }

                    if (value.toString().length > 8) {


                        resText = nome + " : " + value.toString().substring(0, 7)
                    } else {

                        if (entry != null) {
                            resText = nome + " : " + value.toString()
                        }
                    }
                    tvPrice.text = resText

                }
            }
        }else{
            //tvPrice.text = ""

        }
        super.refreshContent(entry, highlight)
    }


    override fun getOffsetForDrawingAtPoint(xpos: Float, ypos: Float): MPPointF {
        return MPPointF(-width / 2f, -height - 10f)
    }

}