
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.orior.R
import com.example.orior.ControllerGrafici.chartController
import com.example.orior.DataCard.datiCard3
import com.github.mikephil.charting.charts.BarChart
import com.github.mikephil.charting.charts.LineChart
import com.github.mikephil.charting.charts.PieChart
import java.text.NumberFormat
import java.util.*
import kotlin.collections.ArrayList


class RvAdapter5(val userList: ArrayList<datiCard3>) : RecyclerView.Adapter<RvAdapter5.ViewHolder>() {

    override fun getItemViewType(position: Int): Int {

        return position
    }

    override fun getItemId(position: Int): Long {
        return super.getItemId(position)
    }
    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): ViewHolder {
        lateinit var v : View

        when(p1) {
            0-> v = LayoutInflater.from(p0?.context).inflate(R.layout.custom_view3, p0, false)
            1-> v = LayoutInflater.from(p0?.context).inflate(R.layout.custom_chart, p0, false)
            2-> v = LayoutInflater.from(p0?.context).inflate(R.layout.custom_rotondo, p0, false)
            in 3..5->  v = LayoutInflater.from(p0?.context).inflate(R.layout.custom_bar, p0, false)
            else -> "fatti in culo"
        }
        return ViewHolder(v);
    }
    override fun getItemCount(): Int {
        return userList.size
    }
    override fun onBindViewHolder(p0: ViewHolder, p1: Int) {
        when (p1) {
            0 ->{
                // p0.tipodato.text = userList[p1].uno
                p0.confermati.text =
                    NumberFormat.getNumberInstance(Locale.ITALIAN).format(userList[p1].confermati?.toInt())
                p0.staticon.text =
                    NumberFormat.getNumberInstance(Locale.ITALIAN).format(userList[p1].confermati_ieri?.toInt())
                p0.guariti.text =
                    NumberFormat.getNumberInstance(Locale.ITALIAN).format(userList[p1].guariti?.toInt())
                p0.statiguar.text =
                    NumberFormat.getNumberInstance(Locale.ITALIAN).format(userList[p1].guariti_ieri?.toInt())
                p0.deceduti.text =
                    NumberFormat.getNumberInstance(Locale.ITALIAN).format(userList[p1].deceduti?.toInt())
                p0.statidec.text =
                    NumberFormat.getNumberInstance(Locale.ITALIAN).format(userList[p1].deceduti_ieri?.toInt())
                if(userList[p1].confermati_ieri?.contains("-")!!){
                    p0.staticon.setTextColor(Color.GREEN)
                }else{
                    p0.staticon.setTextColor(Color.RED)
                    p0.staticon.text = "+"+p0.staticon.text
                }
                if(userList[p1].guariti_ieri?.contains("-")!!){
                    p0.statiguar.setTextColor(Color.RED)
                }else{
                    p0.statiguar.setTextColor(Color.GREEN)
                    p0.statiguar.text = "+"+p0.statiguar.text
                }
                if(userList[p1].deceduti_ieri?.contains("-")!!){
                    p0.statidec.setTextColor(Color.GREEN)
                }else{
                    p0.statidec.setTextColor(Color.RED)
                    p0.statidec.text = "+"+p0.statidec.text
                }
            }
            1 -> {
                chartController(
                    userList[p1].datiRegionali,
                    p0.line,
                    userList[p1].regione as String,
                    p0.contesto
                )
            }
            2-> {
                chartController(
                    userList[p1].datiProvinciali,
                    p0.rotondo,
                    userList[p1].regione as String
                )
            }
            3->{
                chartController(
                    userList[p1].datiRegionali,
                    p0.bar,
                    userList[p1].regione as String,
                    0,
                    p0.contesto,
                    p0.btn1,
                    p0.btn2,
                    p0.btn3
                )
            }
            4->{
                chartController(
                    userList[p1].datiRegionali,
                    p0.bar,
                    userList[p1].regione as String,
                    1,
                    p0.contesto,
                    p0.btn1,
                    p0.btn2,
                    p0.btn3
                )
            }
            5->{
                chartController(
                    userList[p1].datiRegionali,
                    p0.bar,
                    userList[p1].regione as String,
                    2,
                    p0.contesto,
                    p0.btn1,
                    p0.btn2,
                    p0.btn3
                )
            }
            else->"fatti in culo"
        }
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        val confermati = itemView.findViewById<TextView>(R.id.nConfermati)
        val staticon = itemView.findViewById<TextView>(R.id.precCon)
        val guariti = itemView.findViewById<TextView>(R.id.nGuariti)
        val statiguar = itemView.findViewById<TextView>(R.id.precGuar)


        val deceduti = itemView.findViewById<TextView>(R.id.nDeceduti)
        val statidec = itemView.findViewById<TextView>(R.id.preDec)
        val dati = itemView.findViewById<TextView>(R.id.Dato)
        val line = itemView.findViewById<LineChart>(R.id.Line)
        val rotondo = itemView.findViewById<PieChart>(R.id.Circle)
        val bar = itemView.findViewById<BarChart>(R.id.Barra)
        val contesto = itemView.context
        val btn1 = itemView.findViewById<Button>(R.id.settimana)
        val btn2 = itemView.findViewById<Button>(R.id.mese)
        val btn3 = itemView.findViewById<Button>(R.id.anno)
    }
}