import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.core.graphics.drawable.DrawableCompat
import androidx.recyclerview.widget.RecyclerView
import com.example.orior.DataCard.datiCard
import com.example.orior.R
import com.example.orior.ControllerGrafici.chartController
import com.example.orior.DataCard.PassValue

import com.github.mikephil.charting.charts.BarChart
import com.github.mikephil.charting.charts.LineChart
import com.github.mikephil.charting.charts.PieChart
import java.text.NumberFormat
import java.util.*
import kotlin.collections.ArrayList


class RvAdapter(val userList: ArrayList<datiCard>) : RecyclerView.Adapter<RvAdapter.ViewHolder>() {
var flag = false
    override fun getItemViewType(position: Int): Int {

        return position
    }

    override fun getItemId(position: Int): Long {
        return super.getItemId(position)
    }
    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): ViewHolder {
      lateinit var v : View

        when(p1) {
            in 0..2-> v = LayoutInflater.from(p0?.context).inflate(R.layout.custom_view, p0, false)
            3-> v = LayoutInflater.from(p0?.context).inflate(R.layout.custom_chart, p0, false)
            4-> v = LayoutInflater.from(p0?.context).inflate(R.layout.custom_rotondo, p0, false)
            in 5..7->  v = LayoutInflater.from(p0?.context).inflate(R.layout.custom_bar, p0, false)
        else -> "fatti in culo"
        }
        return ViewHolder(v);
    }
    override fun getItemCount(): Int {
        return userList.size
    }
    override fun onBindViewHolder(p0: ViewHolder, p1: Int) {

        when (p1) {
        0 ->{
                p0.dati.setTextColor(Color.parseColor("#5E6D92"))
                p0.tipodato.text = userList[p1].uno
                p0.dati.text =
                    NumberFormat.getNumberInstance(Locale.ITALIAN).format(userList[p1].due?.toInt())
                p0.statistiche.text =
                    NumberFormat.getNumberInstance(Locale.ITALIAN).format(userList[p1].tre?.toInt())
                println(p0.statistiche.text)

                if (!p0.statistiche.text.contains('-')) {
                    p0.statistiche.text = "+" + p0.statistiche.text
                    checkChangeColor(p0.statistiche, p1)

                } else {
                    if (p1 != 2) {
                        val cii = p0.statistiche.background.current
                        val wrappedDrawable = DrawableCompat.wrap(cii)
                        DrawableCompat.setTint(wrappedDrawable, Color.GREEN)
                    }
                } }
             1 ->{
                 p0.dati.setTextColor(Color.parseColor("#F44336"))
                 p0.tipodato.text = userList[p1].uno
                 p0.dati.text =
                     NumberFormat.getNumberInstance(Locale.ITALIAN).format(userList[p1].due?.toInt())
                 p0.statistiche.text =
                     NumberFormat.getNumberInstance(Locale.ITALIAN).format(userList[p1].tre?.toInt())


                 if (!p0.statistiche.text.contains('-')) {
                     p0.statistiche.text = "+" + p0.statistiche.text
                     checkChangeColor(p0.statistiche, p1)
                 } else {
                     if (p1 != 2) {
                         val cii = p0.statistiche.background.current
                         val wrappedDrawable = DrawableCompat.wrap(cii)
                         DrawableCompat.setTint(wrappedDrawable, Color.GREEN)
                     }
                 }}
             2 -> {
                 p0.dati.setTextColor(Color.parseColor("#6BB564"))
                 p0.tipodato.text = userList[p1].uno
                 p0.dati.text =
                NumberFormat.getNumberInstance(Locale.ITALIAN).format(userList[p1].due?.toInt())
            p0.statistiche.text =
                NumberFormat.getNumberInstance(Locale.ITALIAN).format(userList[p1].tre?.toInt())


            if (!p0.statistiche.text.contains('-')) {
                p0.statistiche.text = "+" + p0.statistiche.text
                checkChangeColor(p0.statistiche, p1)
            } else {
                if (p1 != 2) {

                    val cii = p0.statistiche.background.current
                    val wrappedDrawable = DrawableCompat.wrap(cii)
                    DrawableCompat.setTint(wrappedDrawable, Color.GREEN)
                }
            }}
             3 -> {
                 userList[p1].Eu?.let {
                     chartController(
                         it,
                         p0.line,
                         p0.contesto

                     )
                 }
             }
            4-> {
                userList[p1].Eu?.let {
                    chartController(
                        it,
                        p0.rotondo,
                        p0.contesto
                    )
                }
            }
            5->{
                userList[p1].Eu?.let {
                    chartController(
                        it,
                        p0.bar,
                        0,
                        p0.settimana,
                        p0.mese,
                        p0.anno,
                        p0.contesto
                    )
                }
            }
            6->{
                userList[p1].Eu?.let {
                    chartController(
                        it,
                        p0.bar,
                        1,
                        p0.settimana,
                        p0.mese,
                        p0.anno,
                        p0.contesto
                    )
                }
            }
            7->{
                userList[p1].Eu?.let {
                    chartController(
                        it,
                        p0.bar,
                        2,
                        p0.settimana,
                        p0.mese,
                        p0.anno,
                        p0.contesto
                    )
                }
                 }
            else->"fatti in culo"
        }
    }
    fun checkChangeColor (p0 : TextView,index : Int){
        if (index == 2){
            val cii = p0.background.current
            val wrappedDrawable = DrawableCompat.wrap(cii)
            DrawableCompat.setTint(wrappedDrawable, Color.parseColor("#32CD32"))
        }else{
            val cii = p0.background.current
            val wrappedDrawable = DrawableCompat.wrap(cii)
            DrawableCompat.setTint(wrappedDrawable, Color.RED)
        }
    }
    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val tipodato = itemView.findViewById<TextView>(R.id.tipoDato)
        val statistiche = itemView.findViewById<TextView>(R.id.Statistiche)
        val dati = itemView.findViewById<TextView>(R.id.Dato)
        val line = itemView.findViewById<LineChart>(R.id.Line)
        val rotondo = itemView.findViewById<PieChart>(R.id.Circle)
        val bar = itemView.findViewById<BarChart>(R.id.Barra)
        var settimana = itemView.findViewById<Button>(R.id.settimana)
        var mese = itemView.findViewById<Button>(R.id.mese)
        var anno = itemView.findViewById<Button>(R.id.anno)
        var contesto = itemView.context
    }
}