package com.example.orior.Adapter


import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.orior.DataCard.datiCard4
import com.example.orior.DataCard.datiCard5
import com.example.orior.R
import kotlinx.android.synthetic.main.custom_view4.view.*
import kotlinx.android.synthetic.main.custom_view5.view.*
import kotlin.collections.ArrayList


class RvAdapter6(val userList: ArrayList<datiCard5>) : RecyclerView.Adapter<RvAdapter6.ViewHolder>() {

    override fun getItemCount(): Int {
        return userList.size
    }
    var onItemClick: ((testo : String) -> Unit)? = null
    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): ViewHolder {


        val v = LayoutInflater.from(p0?.context).inflate(R.layout.custom_view5, p0, false)

        return ViewHolder(v);
    }


    override fun onBindViewHolder(p0: ViewHolder, p1: Int) {
        p0.regione.text = userList[p1].Nome
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView),View.OnClickListener{
        override fun onClick(v: View) {
            onItemClick?.invoke(userList[adapterPosition].Nome)
        }

        val regione = itemView.findViewById<TextView>(R.id.Linkedin)

        init {
            itemView.Linkedin.setOnClickListener(this)
        }

    }
}