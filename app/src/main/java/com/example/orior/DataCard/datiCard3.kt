package com.example.orior.DataCard

import com.example.orior.DatiJson.DatiNazionali
import com.example.orior.DatiJson.DatiProvincia
import com.example.orior.DatiJson.DatiRegionali

class datiCard3 {
    var confermati : String?
    var confermati_ieri : String?
    var guariti : String?
    var guariti_ieri : String?
    var deceduti : String?
    var deceduti_ieri : String?
    var datiRegionali : Collection<DatiRegionali>?
    var regione : String?
    var datiProvinciali : Collection<DatiProvincia>?
    var datiNazionali : Collection<DatiNazionali>?
    constructor(uno : String?, due : String?, tre : String?, quattro : String?, cinque: String?, sei : String?, sette : Collection<DatiRegionali>?, nomeRegione : String?, otto : Collection<DatiProvincia>?,nove : Collection<DatiNazionali>?){
        this.confermati = uno
        this.confermati_ieri = due
        this.guariti = tre
        this.guariti_ieri = quattro
        this.deceduti = cinque
        this.deceduti_ieri = sei
        this.datiRegionali = sette
        this.regione = nomeRegione
        this.datiProvinciali = otto
        this.datiNazionali = nove
    }

}