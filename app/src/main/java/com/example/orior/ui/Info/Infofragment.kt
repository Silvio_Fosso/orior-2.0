package com.example.orior.ui.Info

import android.content.Intent
import android.graphics.Point
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.FrameLayout
import android.widget.LinearLayout
import android.widget.RelativeLayout
import androidx.activity.addCallback
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.orior.Adapter.RvAdapter6
import com.example.orior.DataCard.datiCard5
import com.example.orior.R


class Infofragment : Fragment() {

    companion object {
        fun newInstance() = Infofragment()
    }

    private lateinit var viewModel: InfofragmentViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        return inflater.inflate(R.layout.info, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val callback = requireActivity().onBackPressedDispatcher.addCallback(this) {
            // Handle the back button event
            activity?.finish()
        }
        val display = activity?.getWindowManager()?.getDefaultDisplay();
        val size =  Point();
        if (display != null) {
            display.getSize(size)
        };
        val recyclerView = view.findViewById<RecyclerView>(R.id.Recyclevie)
        var numero = 0
        recyclerView.layoutManager = LinearLayoutManager(view.context, LinearLayout.VERTICAL, false)
        var Nomi = arrayListOf<String>("Silvio Fosso","Christian Mileto","Marcello De Palo","Andrea Garau","Luigi Mazzarella")
        var lista = arrayListOf<datiCard5>()

        Nomi.forEach {
            lista.add(
                datiCard5(
                    it
                )
            )
        }
        activity?.runOnUiThread {
            val rvAdapter = RvAdapter6(lista)
            recyclerView.adapter = rvAdapter
            rvAdapter.onItemClick={numero ->
                GetLinkedin(numero)
            }
        }
    }
    private fun resizeFragment(
        f: Fragment?,
        newWidth: Int,
        newHeight: Int
    ) {
        if (f != null) {
            val view = f.view
            val p = FrameLayout.LayoutParams(newWidth, newHeight)
            view!!.layoutParams = p
            view.requestLayout()
        }
    }
    fun GetLinkedin(nome : String)
    {
        when (nome){
            "Silvio Fosso" -> {      var uri : Uri = Uri.parse("https://www.linkedin.com/in/silvio-fosso-388278a6/")
                val intent = Intent(Intent.ACTION_VIEW, uri)
                startActivity(intent)}
            "Christian Mileto" -> {
                var uri : Uri = Uri.parse("https://www.linkedin.com/in/christian-mileto-2a8219197/")
                val intent = Intent(Intent.ACTION_VIEW, uri)
                startActivity(intent)
            }
            "Marcello De Palo" -> {
                var uri : Uri = Uri.parse("https://it.linkedin.com/in/marcello-de-palo")
                val intent = Intent(Intent.ACTION_VIEW, uri)
                startActivity(intent)
            }
            "Andrea Garau" -> {
                var uri : Uri = Uri.parse("https://it.linkedin.com/in/andrea-garau-6a3bb5158")
                val intent = Intent(Intent.ACTION_VIEW, uri)
                startActivity(intent)
            }
            "Luigi Mazzarella" -> {
                var uri : Uri = Uri.parse("https://it.linkedin.com/in/iamluigimazzarella")
                val intent = Intent(Intent.ACTION_VIEW, uri)
                startActivity(intent)
            }

        }
    }

}


