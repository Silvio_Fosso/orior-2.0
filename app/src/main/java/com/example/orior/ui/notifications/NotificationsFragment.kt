package com.example.orior.ui.notifications

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import androidx.activity.addCallback
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.orior.Adapter.RvAdapter2
import com.example.orior.DataCard.PassValue
import com.example.orior.DataCard.datiCard2
import com.example.orior.Main4Activity
import com.example.orior.R

class NotificationsFragment : Fragment() {


    fun onBackPressed(): Boolean {
        println("gatto1")
        return true
    }
    var ingle = ArrayList<String>()
    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        val callback = requireActivity().onBackPressedDispatcher.addCallback(this) {
            // Handle the back button event
            activity?.finish()
        }
        val v = inflater.inflate(R.layout.activity_main2,container,false)
        val recyclerView = v.findViewById<RecyclerView>(R.id.Recyclevie)
        recyclerView.layoutManager = LinearLayoutManager(this.context, LinearLayout.VERTICAL, false)
        val dataList = ArrayList<datiCard2>()
        var dati = PassValue.shared.getDati1()

                for (i in 0..dati.confermati.count()-1){
                    var confermati = 0
                    var guariti = 0
                    var deceduti = 0
                    var regione = ""

                        if(dati.confermati[i].stato == ""){
                            if(!dati.giorniItaliani.isNullOrEmpty()){
                                for (j in 0..dati.giorniItaliani!!.count()-1){
                                    if(dati.confermati[i].Region.toString() == dati.giorniinglesi!![j]){
                                        println(dati.confermati[i].Region.toString())
                                        ingle.add(dati.giorniinglesi!![j])
                                        regione = dati.giorniItaliani!![j]
                                    }
                                }
                               //dati.confermati[i].Region.toString()

                            }else{
                                regione = dati.confermati[i].Region.toString()
                            }

                            confermati = dati.confermati[i].totale_casi!!.last()
                            deceduti = dati.deceduti[i].totale_casi!!.last()
                            guariti = dati.guariti[i].totale_casi!!.last()
                            dataList.add(datiCard2(regione,confermati.toString(),deceduti.toString(),guariti.toString()))
                        }

                    }








            activity?.runOnUiThread {
                val rvAdapter = RvAdapter2(dataList)

                recyclerView.adapter = rvAdapter
                rvAdapter.onItemClick = { pos, view ->
                    val b = Bundle()
                    val intent = Intent(this.context, Main4Activity::class.java)
                    if(!ingle.isNullOrEmpty()){ b.putString("regione", ingle.get(pos)) //Your id

                        b.putString("regioneIta",dataList.get(pos).regione)}
                    else{
                        b.putString("regione", dataList.get(pos).regione)
                        b.putString("regioneIta",dataList.get(pos).regione)
                    }

                    intent.putExtras(b) //Put your id to your next Intent
                    startActivity(intent)
                }


            }
        return v
        }


    }

