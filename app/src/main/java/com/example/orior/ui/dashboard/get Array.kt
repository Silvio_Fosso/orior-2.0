
package com.example.orior.ui.dashboard

import com.example.orior.DatiJson.Csv.DatiUE
import com.example.orior.okHttp.okHttp1

class ArrayGet {
    var countries = arrayListOf<String>("Andorra","Albania","Austria","Belgium","Belarus","Bosnia and Herzegovina","Bulgaria","Croatia","Cyprus","Czech Republic","Denmark","Estonia","Faroe Islands","Finland","France","Germany","Gibraltar","Greece","Hungary","Iceland","Ireland","Italy","Kosovo","Latvia", "Lithuania","Luxembourg","Malta","Moldova","Monaco","North Macedonia","Netherlands","Poland","Portugal","Romania","San Marino","Slovakia","Slovenia","Spain","Sweden", "Switzerland","Ukraine","United Kingdom")
    val europaconfermati = arrayListOf<DatiUE>()
    val europaguaritii = arrayListOf<DatiUE>()
    val eurodeceduti = arrayListOf<DatiUE>()
    fun inizializeTableWithContent(callback: (Boolean)->Unit){
        okHttp1.shared.getDatiEuropeiDeceduti { m->
            okHttp1.shared.getDatiEuropeiGuariti { s ->
                okHttp1.shared.getDatiEuropeiConfermati {
                    parse(it, 0)
                    parse(s, 1)
                    parse(m, 2)
                    callback(true)

                }
            }
        }
    }

    fun parse(it : String?,scelta : Int){
        if (it != null) {
            var flag = false
            var riga = it.split("\n")
            riga.forEach {
                var colonne = it.split(",")
                for (i in 0..countries.count().minus(1)) {
                    if (colonne.count() > 1){
                        if ((colonne[1] == countries[i])&&(colonne[0]=="")) {

                            var interi = arrayListOf<Int>()
                            for (j in 4..colonne.count() - 1) {
                                interi.add(colonne[j].toFloat().toInt())
                            }
                            when(scelta){
                                0->{
                                    europaconfermati.add(
                                        DatiUE(
                                            colonne[0],
                                            colonne[1],
                                            colonne[2],
                                            colonne[3],
                                            interi
                                        )
                                    )
                                }
                                1-> {
                                    europaguaritii.add(
                                        DatiUE(
                                            colonne[0],
                                            colonne[1],
                                            colonne[2],
                                            colonne[3],
                                            interi
                                        )
                                    )
                                }
                                2-> {
                                    eurodeceduti.add(
                                        DatiUE(
                                            colonne[0],
                                            colonne[1],
                                            colonne[2],
                                            colonne[3],
                                            interi
                                        )
                                    )
                                }
                            }

                        }
                    }
                }

            }

        }
    }
    fun getArr(callback: (Boolean) -> Unit)
    {

        inizializeTableWithContent {
            callback(it)
        }


    }
    companion object {
        val shared = ArrayGet()
    }


}