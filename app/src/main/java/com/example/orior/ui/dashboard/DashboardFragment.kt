


package com.example.orior.ui.dashboard

import android.annotation.SuppressLint
import android.app.Dialog
import android.app.Notification
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.LinearLayout
import android.widget.PopupWindow
import androidx.activity.addCallback
import androidx.annotation.RequiresApi
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.orior.*
import com.example.orior.Adapter.RvAdapter2
import com.example.orior.DataCard.datiCard2

import com.example.orior.DatiJson.Csv.DatiUE
import com.example.orior.DatiJson.DatiProvincia
import com.example.orior.DatiJson.DatiRegionali
import com.example.orior.R
import com.example.orior.okHttp.okHttp1
import com.example.orior.ui.home.HomeFragment
import com.example.orior.ui.notifications.NotificationsFragment
import com.google.android.gms.maps.*
import com.google.android.gms.maps.model.*
import com.google.android.material.snackbar.Snackbar

class DashboardFragment : Fragment(), OnMapReadyCallback,GoogleMap.OnInfoWindowClickListener {
    lateinit var v: View

    lateinit var prov: Collection<DatiProvincia>
    var regC: Collection<DatiUE>? = null
    lateinit var myDialog : Dialog

    var regG: Collection<DatiUE>? = null

    var  regD: Collection<DatiUE>? = null
    lateinit var inflat : LayoutInflater
    var contain : ViewGroup? = null
    var save : Bundle? = null
    private lateinit var mMap: GoogleMap
    var Scelta = 0
    lateinit var mapFragment: MapView

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val callback = requireActivity().onBackPressedDispatcher.addCallback(this) {
            // Handle the back button event
            activity?.finish()
        }
        inflat = inflater
        save = savedInstanceState
        contain = container
        myDialog = Dialog(this.context!!)
        v = inflater.inflate(R.layout.activity_maps, container, false)

        mapFragment = v.findViewById<MapView>(R.id.map)
        mapFragment.onCreate(savedInstanceState)
        mapFragment.getMapAsync(this)
        return v
    }

    override fun onResume() {

        mapFragment.onResume()
        super.onResume()
    }

    override fun onMapReady(googleMap: GoogleMap) {

        mMap = googleMap
        val BConfermati = v.findViewById<Button>(R.id.settimana)
        val BGuariti = v.findViewById<Button>(R.id.mese)
        val BDeceduti = v.findViewById<Button>(R.id.anno)

        BConfermati.setOnClickListener {
            Scelta = 0
            Crea_Marker_regioni(Scelta)
        }
        BGuariti.setOnClickListener {
            Scelta = 1
            Crea_Marker_regioni(Scelta)
        }
        BDeceduti.setOnClickListener {
            Scelta = 2
            Crea_Marker_regioni(Scelta)
        }
        mMap.moveCamera(CameraUpdateFactory.newLatLng(LatLng(42.35122196, 13.39843823)))
        mMap.setMapStyle(MapStyleOptions.loadRawResourceStyle(this.context, R.raw.style_json))
        FillCollection{
            activity!!.runOnUiThread {
                Crea_Marker_regioni(0)
            }
        }

        var lookup = arrayListOf<LatLng>();
        var flag1 = false
        var Mark: Marker

        // Add a marker in Sydney and move the camera

        mMap.setMinZoomPreference(5f)
        mMap.setMaxZoomPreference(8f)
        mMap.setOnCameraMoveListener {
            mMap.setMinZoomPreference(2f)
            if (mMap.cameraPosition.zoom >= 6f) {

                prov.forEach {

                    val sydney = LatLng(it.lat, it.long)
                    val flag = lookup.filter { latlon ->
                        latlon == sydney
                    }

                    if (flag.size == 0) {
                        flag1 = true
                        lookup.add(sydney)
                        activity?.runOnUiThread {

                            val mark2 =


                            mMap.addMarker(
                                MarkerOptions().position(sydney).title("${it.denominazione_provincia} ").icon(
                                    BitmapDescriptorFactory.defaultMarker(
                                        BitmapDescriptorFactory.HUE_CYAN
                                    )
                                )
                            )
                            mark2.snippet = "${getString(R.string.conf)} : ${it.totale_casi}"



                        }
                    }

                }
            } else {
                if (flag1 == true) {
                    flag1 = false
                    lookup.clear()
                    mMap.clear()

                    Crea_Marker_regioni(Scelta)
                }
            }
        }


        mMap.setOnInfoWindowClickListener(this)


    }

    private fun FillCollection(callback : (Boolean)-> Unit) {

        okHttp1.shared.getDatiProvinciaLatest { s ->

            prov = s

        }
        ArrayGet.shared.getArr {
            regC = ArrayGet.shared.europaconfermati
            regG = ArrayGet.shared.europaguaritii
            regD = ArrayGet.shared.eurodeceduti
            callback(true)
        }
    }

    fun Crea_Marker_regioni(Scelta: Int) {
        mMap.clear()



        when (Scelta) {
            0 -> {
                regC?.forEach {
                    CreateMarkerFill(
                        LatLng(
                            it.Latituidine!!.toDouble(),
                            it.Longitudine!!.toDouble()
                        ), it
                    )
                }

            }
            1 -> {
                regG?.forEach {
                    CreateMarkerFill(
                        LatLng(
                            it.Latituidine!!.toDouble(),
                            it.Longitudine!!.toDouble()
                        ), it
                    )
                }
            }
            2 -> {
                regD?.forEach {
                    CreateMarkerFill(
                        LatLng(
                            it.Latituidine!!.toDouble(),
                            it.Longitudine!!.toDouble()
                        ), it
                    )
                }

            }

        }
    }






    fun CreateMarkerFill(sydney: LatLng, it : DatiUE)
    {
        activity?.runOnUiThread {
            when (Scelta) {
                0 -> {
                    val mark = mMap.addMarker(
                        MarkerOptions().position(sydney).title("${it.Region}").icon(
                            BitmapDescriptorFactory.defaultMarker(220f)
                        )
                    )
                    mark.snippet = "${getString(R.string.conf)} : ${it.totale_casi!!.last()}"

                }
                1 -> {
                    val mark = mMap.addMarker(
                        MarkerOptions().position(sydney).title("${it.Region}").icon(
                            BitmapDescriptorFactory.defaultMarker(90.0f)
                        )
                    )
                    mark.snippet = "${getString(R.string.guar)} : ${it.totale_casi!!.last()}"

                }
                2 -> {
                    val mark = mMap.addMarker(
                        MarkerOptions().position(sydney).title("${it.Region}").icon(
                            BitmapDescriptorFactory.defaultMarker(3f)
                        )
                    )
                    mark.snippet = "${getString(R.string.dec)}  : ${it.totale_casi!!.last()}"


                }
            }

        }

    }



    @SuppressLint("ResourceType")
    @RequiresApi(Build.VERSION_CODES.N)
    override fun onInfoWindowClick(p0: Marker?) {



    }
    companion object {
        val shared = DashboardFragment()
    }

}