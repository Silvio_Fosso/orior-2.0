package com.example.orior.DatiJson

import com.google.gson.annotations.SerializedName


     class DatiNazionali (

        @SerializedName("data") val data : String,
        @SerializedName("stato") val stato : String,
        @SerializedName("ricoverati_con_sintomi") val ricoverati_con_sintomi : Int,
        @SerializedName("terapia_intensiva") val terapia_intensiva : Int,
        @SerializedName("totale_ospedalizzati") val totale_ospedalizzati : Int,
        @SerializedName("isolamento_domiciliare") val isolamento_domiciliare : Int,
        @SerializedName("totale_attualmente_positivi") val totale_attualmente_positivi : Int,
        @SerializedName("nuovi_attualmente_positivi") val nuovi_attualmente_positivi : Int,
        @SerializedName("dimessi_guariti") val dimessi_guariti : Int,
        @SerializedName("deceduti") val deceduti : Int,
        @SerializedName("totale_casi") val totale_casi : Int,
        @SerializedName("tamponi") val tamponi : Int,
        @SerializedName("note_it") val note_it : String,
        @SerializedName("note_en") val note_en : String
    )
