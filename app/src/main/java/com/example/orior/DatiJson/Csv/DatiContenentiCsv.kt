package com.example.orior.DatiJson.Csv

class DatiArray{
    var confermati : ArrayList<DatiUE>
    var deceduti : ArrayList<DatiUE>
    var guariti : ArrayList<DatiUE>
    var giorni : ArrayList<String>
    var giorniItaliani : ArrayList<String>?
    var giorniinglesi : ArrayList<String>?
    constructor(confer : ArrayList<DatiUE>,guari : ArrayList<DatiUE>,dece : ArrayList<DatiUE>,nomi : ArrayList<String>,italiano : ArrayList<String>?,inglesi : ArrayList<String>?){
        this.confermati = confer
        this.deceduti = dece
        this.guariti = guari
        this.giorni = nomi
        this.giorniItaliani = italiano
        this.giorniinglesi = inglesi
    }

}