package com.example.orior.DatiJson

import com.google.gson.annotations.SerializedName

class News (
    @SerializedName("body") val body : String,
    @SerializedName("dateTime") val dateTime : String,
    @SerializedName("link") val link : String,
    @SerializedName("title") val title : String

)