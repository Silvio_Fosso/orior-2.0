package com.example.orior




import RvAdapter5
import android.annotation.SuppressLint
import android.graphics.Color
import android.graphics.Point
import android.graphics.drawable.ColorDrawable
import android.icu.text.SimpleDateFormat
import android.os.Build
import android.os.Bundle
import android.widget.LinearLayout
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.orior.DataCard.PassValue
import com.example.orior.DataCard.datiCard3
import com.example.orior.DatiJson.DatiRegionali
import com.example.orior.R
import com.example.orior.okHttp.okHttp1
import java.util.*
import kotlin.collections.ArrayList


class RegioniActivity : AppCompatActivity() {
    val dataList = ArrayList<datiCard3>()
    @RequiresApi(Build.VERSION_CODES.N)
    @SuppressLint("WrongConstant")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main2)
        val display = getWindowManager().getDefaultDisplay();
        val size =  Point();
        display.getSize(size);
        val width = size.x
        window.setLayout(size.x-100,size.y-200)

        window.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

        val recyclerView = findViewById<RecyclerView>(R.id.Recyclevie)
        recyclerView.layoutManager = LinearLayoutManager(this, LinearLayout.VERTICAL, false)



        val nomeRegione = intent.extras?.getString("regione")
        this.setTitle(nomeRegione)
        okHttp1.shared.getDatiProvincia { s->
            okHttp1.shared.getDatiRegionali {

               val giorni =  get_yesterdayDate1(it.last().data.substring(0,10),(it.count()/21)-1)
                PassValue.shared.setDate1(giorni)
                val confermati = getConfermati(it, nomeRegione as String)
                val guariti = getGuariti(it, nomeRegione)
                val morti = getDeceduti(it, nomeRegione)
                dataList.add(
                    datiCard3(
                        confermati.get(0),
                        confermati.get(1),
                        guariti.get(0),
                        guariti.get(1),
                        morti.get(0),
                        morti.get(1),
                        null,
                        null,
                        null,null
                    )
                )
                dataList.add(
                    datiCard3(
                        null,
                        null,
                        null,
                        null,
                        null,
                        null,
                        it,
                        nomeRegione,
                        null,null
                    )
                )
                dataList.add(
                    datiCard3(
                        null,
                        null,
                        null,
                        null,
                        null,
                        null,
                        null,
                        nomeRegione,
                        s,null
                    )
                )
                dataList.add(
                    datiCard3(
                        null,
                        null,
                        null,
                        null,
                        null,
                        null,
                        it,
                        nomeRegione,
                        null,null
                    )
                )
                dataList.add(
                    datiCard3(
                        null,
                        null,
                        null,
                        null,
                        null,
                        null,
                        it,
                        nomeRegione,
                        null,null
                    )
                )
                dataList.add(
                    datiCard3(
                        null,
                        null,
                        null,
                        null,
                        null,
                        null,
                        it,
                        nomeRegione,
                        null,null
                    )
                )
                runOnUiThread {
                    val rvAdapter = RvAdapter5(dataList)
                    recyclerView.adapter = rvAdapter
                }
            }
        }
    }


    @RequiresApi(Build.VERSION_CODES.N)
    fun getConfermati(ita : Collection<DatiRegionali>, nomeRegio : String) : ArrayList<String> {
        val oggi = ita.last().data.substring(0,10)
        val ieri = get_yesterdayDate(oggi)
        val arr = ArrayList<String>()
        ita.forEach {
            if(it.denominazione_regione == nomeRegio && it.data.contains(oggi)){
                val ie = get_ieri(it.totale_casi,ieri,ita,nomeRegio,0)
                arr.add(it.totale_casi.toString())
                arr.add(ie.toString())
                return arr
            }
        }
        return ArrayList<String>()
    }
    fun get_ieri(dato : Int, ieri : String, it:Collection<DatiRegionali>, nom : String, scelta : Int) : Int{
        it.forEach {
            when(scelta){
                0->{
                    if(it.denominazione_regione == nom && it.data.contains(ieri)){
                        return dato.minus(it.totale_casi)
                    }
                }
                1->{
                    if(it.denominazione_regione == nom && it.data.contains(ieri)){
                        return dato.minus(it.dimessi_guariti)
                    }
                }
                2->{
                    if(it.denominazione_regione == nom && it.data.contains(ieri)){
                        return dato.minus(it.deceduti)
                    }
                }
            }

        }
        return 0
    }


    @RequiresApi(Build.VERSION_CODES.N)
    fun get_yesterdayDate(data : String) : String{
        val dateFormat: SimpleDateFormat = SimpleDateFormat("yyyy-MM-dd")
        val cal = Calendar.getInstance()
        val date: Date = dateFormat.parse(data)
        cal.time = date
        cal.add(Calendar.DATE, -1)
        return dateFormat.format(cal.time).toString()
    }
    @RequiresApi(Build.VERSION_CODES.N)
    fun get_yesterdayDate1(data : String, intero : Int) : ArrayList<String>{
        val i = 0
        val ar = arrayListOf<String>()
        for(i in 0..intero) {
            Thread.yield()
            val dateFormat: android.icu.text.SimpleDateFormat =
                android.icu.text.SimpleDateFormat("yyyy-MM-dd")
            val cal = Calendar.getInstance()
            val date: Date = dateFormat.parse(data)
            cal.time = date
            cal.add(Calendar.DATE, -i)
            ar.add(dateFormat.format(cal.time).toString())
        }
        return ar
    }
    @RequiresApi(Build.VERSION_CODES.N)
    fun getDeceduti(ita : Collection<DatiRegionali>, nomeRegio : String) : ArrayList<String> {
        val oggi = ita.last().data.substring(0,10)
        val ieri = get_yesterdayDate(oggi)
        val arr = ArrayList<String>()
        ita.forEach {
            if(it.denominazione_regione == nomeRegio && it.data.contains(oggi)){
                val ie = get_ieri(it.deceduti,ieri,ita,nomeRegio,2)
                arr.add(it.deceduti.toString())
                arr.add(ie.toString())
                return arr
            }
        }
        return ArrayList<String>()
    }
    @RequiresApi(Build.VERSION_CODES.N)
    fun getGuariti(ita : Collection<DatiRegionali>, nomeRegio : String) : ArrayList<String> {
        val oggi = ita.last().data.substring(0,10)
        val ieri = get_yesterdayDate(oggi)
        val arr = ArrayList<String>()
        ita.forEach {
            if(it.denominazione_regione == nomeRegio && it.data.contains(oggi)){
                val ie = get_ieri(it.dimessi_guariti,ieri,ita,nomeRegio,1)
                arr.add(it.dimessi_guariti.toString())
                arr.add(ie.toString())
                return arr
            }
        }
        return ArrayList<String>()
    }
}
